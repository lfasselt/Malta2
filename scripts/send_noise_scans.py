#!/usr/local/bin/python

# the script runs a set of pre-configured runs for different vsub and ithr values

import os, glob

chip="W18R9"
ITHRs = [
         15, 
#         20, 
#         30, 
#         40, 
#         60, 
#         80, 
#         100, 
]

VSUBs = [
#         6, 
#         10, 
#         15,
#         20, 
#         25,
#         30, 
#         35,
         40, 
#         45,
#         50,
#         55
]
'''
chip="W18R10"
ITHRs = [
         15, 
         20, 
         30, 
         40, 
         60, 
         80, 
         100, 
]

VSUBs = [
#         6, 
#         10, 
#         15,
#         20, 
#         25,
#         30, 
#         35,
#         40, 
#         45,
         50,
         55
]


chip="W12R8"
ITHRs = [
#         20, 
         40, 
#         60, 
#         80, 
#         100, 
#         120
]

VSUBs = [
#         6, 
#         8, 
         10, 
#         12, 
#         14
]


chip="W18R21"
ITHRs = [
#15, 
#         20, 
#         40, 
#         60, 
#         80, 
         100, 
         120
]

VSUBs = [
         6, 
         10, 
         15,
         20, 
         25,
         30, 
         35,
         40, 
         45,
         50,
         55
]
    

#chip="W15R0"
#ITHRs = [#18, 
#          20, 
#         30, 
#         40, 
##         50, 
#         60, 
##         70, 
#         80, 
##         90, 
#         100, 
##         110, 
#         120
#]

#VSUBs = [
#         6, 
#         7, 
#         8, 
#         9, 
#         10, 
#         11, 
#         12
#]
    

chip="W15R19"
ITHRs = [
          15, 
#          18, 
#          20, 
#         30, 
#         40, 
#         50, 
#         60, 
#         70, 
#         80, 
#         90, 
#         100, 
#         110, 
#         120
]

VSUBs = [
#    6, 
    10, 
#    15, 
#    20,          
#    25, 
#    30, 
#    32
]

chip="W14R3"
ITHRs = [15, 20, 40, 
         #50, 
         60, 
         #70, 
         80, 
         #90, 
         100, 
         #110, 
         120]
VSUBs = [6, 10, 15, 20, 25, 30, 35, 40, 45, 50]
#chip="W14R12"
#ITHRs = [#12, 
#         15, 
#         20, 
#         #30, 
#         40, 
#         #50, 
#         60, 
#         #70, 
#         80, 
#         #90, 
#         100, 
#         #110, 
#         120
]
VSUBs = [
#    6, 
#    6.5, 
#    7, 
#    7.5, 
#    8, 
#    8.5, 
#    9
]
'''

#t = 5e4 * 20

address="udp://ep-ade-gw-04.cern.ch:50002"

#set IDB scan range
paramMin = 120
paramMax = 121
paramStep = 10

for VSUB in VSUBs:

    #    print("MALTA_PSU.py -c rampVoltage SUB %.1f 10" % float(VSUB))
    os.system("MALTA_PSU.py -c rampVoltage SUB %.1f 10" % float(VSUB))

    for ITHR in ITHRs:

        #        configs = glob.glob("configs/sps_config/%s/config_telescope_SPS_DUT_%s_120%d.txt" % (chip, chip, ITHR))
        #        configs = glob.glob("configs/sps_config/%s/config_telescope_SPS_DUT_%s_120%d*.txt" % (chip, chip, ITHR))
        #        configs = glob.glob("configs/sps_config/%s/config_telescope_SPS_DUT_%s_120%d*.txt" % (chip, chip, ITHR))
        #        print(configs)
        #configs = glob.glob("configs/sps_config/%s/*ITHR%d*" % (chip, ITHR))
        configs = glob.glob("configs/sps_config/%s/*ITHR%d*31pixelmask*" % (chip, ITHR))
        #        configs = glob.glob("configs/sps_config/%s/*ITHR%d*" % (chip, ITHR))

        for config in configs:

            npixmask = ""            
            ndcmask = ""            
            words = config.split("_")

            #for word in words:
            #    if "pixelmask" in word: npixmask = word.replace('.txt','')
            #    if "dcolmask" in word: ndcmask = word.replace('.txt','')

            n_pixelmask = 0
            n_dcolmask = 0
            with open(config, "r") as c:
                for line in c.readlines():
                    if "SC_IBIAS" in line: IBIAS = int(line.split(" ")[-1].replace("\n",""))
                    if "SC_ITHR" in line: ITHR = int(line.split(" ")[-1].replace("\n",""))
                    if "SC_IDB" in line: IDB = int(line.split(" ")[-1].replace("\n",""))
                    if "SC_VCASN" in line: VCASN = int(line.split(" ")[-1].replace("\n",""))
                    if "SC_VRESET_P" in line: VRESET_P = int(line.split(" ")[-1].replace("\n",""))
                    if "SC_VRESET_D" in line: VRESET_D = int(line.split(" ")[-1].replace("\n",""))
                    if "SC_VCLIP" in line: VCLIP = int(line.split(" ")[-1].replace("\n",""))
                    if "MASK_PIXEL" in line and not "#MASK_PIXEL" in line: n_pixelmask += 1
                    if "MASK_DOUBLE_COLUMN" in line and not "#MASK_DOUBLE_COLUMN" in line: n_dcolmask += 1

            if IBIAS != 43: 
                print ("\nINFO :: IBIAS VALUE %d != 43...skipping!!!\n" % IBIAS)
                continue

            tag = "20230118_%s_IBIAS_%d_ITHR_%d_IDB_%d_ICASN_2_VCASN_%d_VRESET_P_%d_VRESET_D_%d_NoiseScan" % (chip, IBIAS, ITHR, IDB, VCASN, VRESET_P, VRESET_D)
            tag += ("_VSUB_%.1f" % float(VSUB)).replace('.','p')
            #            if npixmask: tag += "_" + npixmask
            #            if ndcmask: tag += "_" + ndcmask
            if n_pixelmask: tag += "_%dpixelmask" % n_pixelmask
            if n_dcolmask: tag += "_%ddcolmask" % n_dcolmask
            #            tag += "_postSPS"

            cmd = "Malta2NoiseScan " 
            cmd += " -a "+address # address
            cmd += " -o "+tag #output
            cmd += " -f "+chip # basefolder
            cmd += " -l "+str(paramMin)  # paramMin
            cmd += " -h "+str(paramMax)  # paramMax
            cmd += " -s "+str(paramStep) # paramStep
            cmd += " -r 200 " # repetitions
            cmd += " -n 100000" # maximum number of noisy pixels
            cmd += " -t 5000" # trigger
            cmd += " -q" # quiet
            #cmd += " -v" # verbose
            #cmd += " -c configs/Malta2testSPSGiuliano.txt" # config file
            cmd += " -c " + config
            #            print(cmd)
            os.system(cmd)

