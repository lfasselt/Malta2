#!/usr/local/bin/python

"""
run_analog_scan.py

This script runs the analog scan for MALTA2

MLB <matt.leblanc@cern.ch>
"""

import os
import datetime

now=datetime.datetime.now()
now_st="Stop_"###now.strftime("%Y%m%d__%H_%M_%S")

chip="W8R20"

chip="W5R10"
#chip="W18R18"
address="udp://ep-ade-gw-07.cern.ch:50001"


paramMin = 10
paramMax = 90
tag = now_st+"_"+chip+"_RATETEST_Min"+str(paramMin)+"_Max"+str(paramMax)

cmd = "Malta2AnalogScan " 
cmd += " -a "+address # address
cmd += " -o "+tag #output
cmd += " -f "+chip # basefolder
cmd += " -l "+str(paramMin) # paramMin
cmd += " -h "+str(paramMax) # paramMax

#cmd += " -r 0 512 400 2" # region [xmin(0-511) No.x ymin(>287) No.y (0-223)]
cmd += " -r 0 5 288 224" # region [xmin(0-511) No.x ymin(>287) No.y (0-223)]
#cmd += " -r 0 20  288 224 " # region [xmin(0-511) No.x ymin(>287) No.y (0-223)]
#cmd += " -r 350 25  291 1 " # region [xmin(0-511) No.x ymin(>287) No.y (0-223)]
#cmd += " -r 0 512  291 2 " # region [xmin(0-511) No.x ymin(>287) No.y (0-223)]
##cmd += " -r 330 70  288 1 " # region [xmin(0-511) No.x ymin(>287) No.y (0-223)]
#cmd += " -c configs/Malta2test.txt" # config file
#cmd += " -t 80" # trigger
cmd += " -t 100" # trigger
cmd += " -q" # quiet
cmd += " -c configs/SPS.txt" # config file
#cmd += " -e" # only pulse even pixels in X
#cmd += " -y" # pulse with y=y+offset
#cmd += " -g" # pulse in 2x4 blocks
#cmd += " -c" # config file
#cmd += " -T" # tap file
print(cmd)
os.system(cmd)


'''
Unmask the desired pixel: 226,288== 
 VALERIO: 225 , 288

Unmask the desired pixel: 262,288==
 VALERIO: 261 , 288

Unmask the desired pixel: 312,288==
 VALERIO: 311 , 288

Unmask the desired pixel: 324,288==
 VALERIO: 323 , 288

Unmask the desired pixel: 420,288==
 VALERIO: 419 , 288

Unmask the desired pixel: 432,288==
 VALERIO: 431 , 288

Unmask the desired pixel: 468,288==
 VALERIO: 467 , 288

#####################################################################
 #unstability!!!!

Unmask the desired pixel: 428,288==
 VALERIO: 428 , 288

Unmask the desired pixel: 429,288==
 VALERIO: 429 , 288

Unmask the desired pixel: 430,288==
 VALERIO: 430 , 288

Unmask the desired pixel: 431,288==
 VALERIO: 430 , 288

Unmask the desired pixel: 505,288==
 VALERIO: 505 , 288   ===> unstable!!!!

'''
