#!/usr/local/bin/python

import os
import glob
import os.path
from os import path

#dList=glob.glob("/home/sbmuser/MaltaSW/Malta2/Results_ThScan_ValerioCleaning/*/*")
dList=glob.glob("/home/sbmuser/MaltaSW/data/Malta2/Results_ThScan/*/*")

exclude=[] #"W18R19"]

for d in dList:
    passL=False
    for e in exclude:
        if e in d: passL=True
    if passL: continue
    
    #print (d)
    ng=glob.glob(d+"/*.eps")
    if len(ng)!=0:
        command1="rm "+d+"/*.eps "
        #print (command1)
        os.system(command1)
    res2=d+"/hists.tgz"    
    res=d+"/hists"
    if not path.exists(res):
        if path.exists(res2):
            print (d+" already contains a hist.tgz file .... likely cleaning already ran")
            continue 
        print (d+" does not contain a hist folder .... likely aborted scan ... consider deleting")
        continue
    command2="cd "+d+"/; tar -zcf hists.tgz hists; cd - "
    #print (command2)
    os.system(command2)
    command3="rm -rf "+res
    #print (command3)
    os.system(command3)
    
        
