#ifndef MALTA2TREE_H
#define MALTA2TREE_H

#include "Malta2/Malta2Data.h"
#include "TFile.h"
#include "TTree.h"
#include <string>
#include <chrono>

/**
 * Malta2Tree is a tool to store MALTA2 data as a ROOT TTree.
 * A new file can be openned with Malta2Tree::Open. 
 * Parameters that will be stored for every entry until 
 * changed are run number (Malta2Tree::SetRunNumber),
 * ITHRES (Malta2Tree::SetIthres), L1ID (Malta2Tree::SetL1idC),
 * duplicate flag (Malta2Tree::SetIsDuplicate).
 * Malta2Data (Malta2Tree::Set) will be decoded and stored as
 * different branches in the tree, as well as a time stamp
 * (timer). After adding the parameters, it is necessary 
 * to call Malta2Tree::Fill.
 * Malta2Data can be written like the following:
 *
 * @verbatim
   Malta2Tree * mt = new Malta2Tree();
   mt->Open("file.root","RECREATE");

   mt->SetIthr(200);
   mt->SetRunNumber(5001);
 
   for(...){
     Malta2Data * md=...
     mt->Add(md);
     mt->Fill();
   }
   
   mt->Close();
   delete mt;
   @endverbatim
 *
 * Malta2Data can be read like the following:
 *
 * @verbatim
   Malta2Tree * mt = new Malta2Tree();
   mt->Open("file.root","READ");
 
   while(mt->Next()){
     Malta2Data * md = mt->Get()
     uint32_t l1id = mt->GetL1idC();
     float timer = mt->GetTimer();
   }
   
   mt->Close();
   delete mt;
   @endverbatim
 *
 * @brief Tool to store Malta2Data as a ROOT TTree.
 * @author Carlos.Solans@cern.ch
 * @date October 2018
 **/

class Malta2Tree{

 public:
  
  /**
   * @brief Create an empty Malta2Tree.
   **/
  Malta2Tree();
  
  /**
   * @brief Delete the Malta2Tree. Close any open file.
   **/
  ~Malta2Tree();

  /**
   * @brief Open a ROOT file.
   * @param filename The path to the file as a string.
   * @param options ROOT options for the file.
   **/
  void Open(std::string filename,std::string options);
  
  /**
   * @brief Close the ROOT file.
   **/
  void Close();

  /**
   * @brief Extend the tree to hold calibration data
   **/
  void Extend();
  
  /**
   * @brief Add a new entry to the Tree.
   **/
  void Fill();
  
  /**
   * @brief Add the contents of Malta2Data to the current entry.
   *        This also adds a time stamp (timer) to the entry.
   * @param data The Malta2Data that needs to be stored in the tree entry.
   **/
  void Set(Malta2Data * data);
  
  /**
   * @brief Set the run number in the current entry.
   * @param v The value of the run number.
   **/
  void SetRunNumber(uint32_t v);

  /**
   * @brief Mark the MALTA word as duplicate.
   * @param v 1 if duplicate, 0 if not.
   **/
  void SetIsDuplicate(uint32_t v);

  /**
   * @brief Set the L1ID in the current entry.
   * @param v The value of the L1ID.
   **/
  void SetL1idC(uint32_t v);

  /**
   * @brief Set the IDB in the current entry.
   * @param v The value of the IDB.
   **/
  void SetIDB(uint32_t v);

  /**
   * @brief Set the ITHR in the current entry.
   * @param v The value of the ITHR.
   **/
  void SetITHR(uint32_t v);

  /**
   * @brief Set the VLOW in the current entry.
   * @param v The value of the VLOW.
   **/
  void SetVLOW(uint32_t v);

  /**
   * @brief Set the VHIGH in the current entry.
   * @param v The value of the VHIGH.
   **/
  void SetVHIGH(uint32_t v);

  /**
   * @brief Get the current entry as a Malta2Data object.
   * @return A Malta2Data object.
   **/
  Malta2Data * Get();

  /**
   * @brief Get the run number in the current entry.
   * @return The value of the run number.
   **/
  uint32_t GetRunNumber();

  /**
   * @brief Get the duplicate flag of the entry.
   * @return The duplicate flag.
   **/
  uint32_t GetIsDuplicate();

  /**
   * @brief Get the L1ID of the current entry.
   * @return The value of the L1ID.
   **/
  uint32_t GetL1idC();

  /**
   * @brief Get the IDB in the current entry.
   * @return The value of the IDB.
   **/
  uint32_t GetIDB();

  /**
   * @brief Get the ITHR in the current entry.
   * @return The value of the ITHR.
   **/
  uint32_t GetITHR();

  /**
   * @brief Get the VLOW in the current entry.
   * @return The value of the VLOW.
   **/
  uint32_t GetVLOW();

    /**
   * @brief Get the VHIGH in the current entry.
   * @return The value of the VHIGH.
   **/
  uint32_t GetVHIGH();

  /**
   * @brief Get the timestamp of the entry.
   * @return The timestamp of the run number.
   **/
  float GetTimer();

  /**
   * @brief Move the internal pointer to the next entry in the tree.
   * @return 0 if there are no more entries to read, -1 if there was an error.
   **/
  int Next();

  /**
   * @brief Get underlaying ROOT file used by Malta2Tree.
   * @return The ROOT TFile object.
   **/
  TFile* GetFile();

 private:
  
  TFile * m_file;
  TTree * m_tree;
  bool m_readonly;
  bool m_extended;
  std::chrono::steady_clock::time_point m_t0;
  
  uint32_t pixel;
  uint32_t group;
  uint32_t parity;
  uint32_t delay;
  uint32_t dcolumn;
  uint32_t chipbcid;
  uint32_t chipid;
  uint32_t phase;
  uint32_t winid;
  uint32_t bcid;
  uint32_t l1id;
  uint32_t run;
  uint32_t l1idC;
  float    timer;
  uint32_t isDuplicate;
  uint32_t idb;
  uint32_t ithr;
  uint32_t vlow;
  uint32_t vhigh;

  Malta2Data m_data;
  uint64_t m_entry;
};

#endif
