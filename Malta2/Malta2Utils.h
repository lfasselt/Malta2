#ifndef ALTA2UTILS_H
#define MALTA2UTILS_H

#include "Malta2/Malta2.h"

#include "ipbus/Uhal.h"

// C++
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>
#include <vector>

// ROOT
#include <TH2I.h>
#include <TH1D.h>

/**
 * Malta2Utils contains common functions 
 * which are used to configure the
 * MALTA2 Pixel detector prototype.
 *
 * @brief MALTA2 common utilities
 * @author Andrea.Gabrielli@cern.ch
 * @author Matt.LeBlanc@cern.ch
 * @date April 2021
 **/

using namespace std;

namespace Malta2Utils 
{  
  // Function to pre-configure MALTA22
  void PreConfigureMalta(Malta2* malta, const int sleep1=50000, const int sleep2=100000);

  // Masks all channels on MALTA2
  void MaskAll(Malta2* malta);
  
  // Un-masks all channels on MALTA2
  void EnableAll(Malta2* malta);

  // Send MALTA2 commands
  void MaltaSend(Malta2* malta, const int sleep1=500, const int sleep2=1000);

  // Make a system call
  stringstream SystemCall(string cmd);

  // Duplication Removal
  bool markDuplicate(uint32_t bcid1,
		     uint32_t bcid2,
		     uint32_t winid1,
		     uint32_t winid2,
		     uint32_t l1id1,
		     uint32_t l1id2,
		     uint32_t phase1,
		     uint32_t phase2);
  bool isDuplicateNext(uint32_t bcid1,
		       uint32_t bcid2,
		       uint32_t winid1,
		       uint32_t winid2,
                       uint32_t l1id1,
		       uint32_t l1id2,
		       uint32_t phase1,
		       uint32_t phase2);
}

#endif
