#ifndef MALTA2_MALTA2THRESHOLDANALYSIS_H
#define MALTA2_MALTA2THRESHOLDANALYSIS_H

#include "Malta2/Malta2Data.h"
#include "TH1F.h"
#include "TTree.h"
#include "TFile.h"
#include "TGraphErrors.h"
#include <string>
#include <map>

/**
 * @brief MALTA2 threshold scan analysis algorithm
 * @todo: Update documentation
 * @author: ported to MALTA2 by MLB <matt.leblanc@cern.ch>
 * @date: 2021/04/01
 **/
class Malta2ThresholdAnalysis{

 public:

  Malta2ThresholdAnalysis(uint32_t nbins, float minVal, float maxVal, std::string folderName);
  
  ~Malta2ThresholdAnalysis();

  void process(Malta2Data *md, uint32_t orig_column, uint32_t new_column, uint32_t row, float param, bool isDuplicate);

  bool check_next_empty(int , TH1F* );  
  double find_middle_point(TGraphErrors *);

  void end(float Scantime,float mScantime_pix[], TTree* config_tree);
  
  void setNPulse(int pulse);

  float getValueEl(int VL, int VH);

 private:

  TH1F * m_histos[512][512];
  TH1F * m_hThres;
  uint32_t m_nbins;
  float m_minVal;
  float m_maxVal;
  TFile * m_file;
  int m_npulse;
  std::string m_folderName; 

  std::map<int,float> m_conversion;
};


#endif

