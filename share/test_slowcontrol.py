#!/usr/bin/env python
import PyMalta2
import time
import signal
import argparse
import random
import sys
import classmonitor
import Keithley
from ROOT import TCanvas, TFile, TH1F, TH2F, gStyle, TGraph, TTree
from array import array


#Test declaration of arguments needed to run it
parser = argparse.ArgumentParser(description='Hi there')
parser.add_argument("-r","--repetitions", help='number of repetitions', type=int, default=1)
parser.add_argument("--gw", help='Gateaway router number:7 or 4"', type=int, required=True)
parser.add_argument("-v","--verbose",help="verbose mode",action='store_true')
parser.add_argument("-R","--RESET",help="ONLY reset the chip",action='store_true')
parser.add_argument("-m","--monitor",help="monitor DACs",action='store_true')
parser.add_argument("--SET_VPULSE_LOW", help='SET_VPULSE_LOW default is 10', type=int, default=10)
parser.add_argument("--SET_VCASN",      help='SET_VCASN default is 110'    , type=int, default=110)
parser.add_argument("--SET_VCLIP",      help='SET_VCLIP default is 125'    , type=int, default=125)
parser.add_argument("--SET_VPULSE_HIGH",help='SET_VPULSE_HIGH default is 90',type=int, default=90)

    
args = parser.parse_args()
repetitions=args.repetitions
v=args.verbose
print("Verbose: %s" % str(v))
skip_FIFOs=False
monitorDACs=args.monitor

SET_VPULSE_LOW=int(args.SET_VPULSE_LOW)
SET_VPULSE_HIGH=int(args.SET_VPULSE_HIGH)
SET_VCASN=int(args.SET_VCASN)
SET_VCLIP=int(args.SET_VCLIP)

m=PyMalta2.Malta2()
st_connect="udp://ep-ade-gw-0%i:50001" % args.gw
m.Connect(st_connect)
print("************************************")
print("You are calling to %s" % st_connect)
m.Connect(st_connect)
print("************************************")
time.sleep(0.1)
m.SetVerbose(v)

#print("m.Word()")
#print(m.Word("SET_ICASN"))


cont = True
def signal_handler(signal, frame):
    print('You pressed ctrl+C')
    global cont
    cont = False
    return

signal.signal(signal.SIGINT, signal_handler)

def test_defaults():
    # SetDefaults with true calls WriteAll method
    m.SetDefaults(True)
    time.sleep(0.01)
    # resets everything on FPGA
    m.SetState(0)
    time.sleep(0.01)
    # state 1 starts test
    m.SetState(1)
    time.sleep(0.1)
    # reads response from emulator
    m.ReadAll()
    time.sleep(0.1)
    # print on screen result. False because I just ReadAll()
    return m.Dump(False)

def test_changing_settings():
    #SetDefaults with false! So I will need to WriteAll()
    m.SetDefaults(False)
    for  x in range(17,62):#17-62 is the range of all the small (non-fifo) words
        m.Write(x,1,False)# put them in 1 to test
        pass
    m.WriteAll()# needed because setdefaults is set to False
    time.sleep(0.1)
    # resets everything on FPGA
    m.SetState(0)
    time.sleep(0.01)
    # start test
    m.SetState(1)
    time.sleep(0.2)
    return m.Dump(True)

def test_changing_settings_FIFO(x):
    global SET_VPULSE_LOW
    global SET_VPULSE_HIGH
    global SET_VCASN
    global SET_VCLIP
    #SetDefaults with false! So I will need to WriteAll()
    m.Reset()
    m.SetDefaults(False)

    

    #for  x in range(17,62):#17-62 is the range of all the small (non-fifo) words
    #    m.Write(x,0,False)# put them in 1 to test
    #    pass
    #101000....00001
    #m.Write(m.Word("DATAFLOW_MERGERTOLEFT"),1,False)
    #m.Write(m.Word("DATAFLOW_MERGERTORIGHT"),0,False)
    #m.Write(m.Word("DATAFLOW_LMERGERTOLVDS"),1,False)
    m.Write(m.Word("LVDS_VBCMFB")      ,0xC,False)
    m.Write(m.Word("DATAFLOW_ENMERGER"),0x0,False)
    m.Write(m.Word("SET_ITHR")         ,120,False)# put them in 1 to test
    m.Write(m.Word("SET_IDB")          ,120,False)# put them in 1 to test
    #
    '''
    m.Write(m.Word("PULSE_MON_L"),1,False)
    m.Write(m.Word("PULSE_MON_R"),1,False)
    m.Write(m.Word("SWCNTL_DACMONV"),1,False)
    m.Write(m.Word("SWCNTL_DACMONI"),1,False)
    #m.Write(m.Word("SET_IRESET_BIT"),1,False)
    time.sleep(0.5)
    # hot encoding
    print("--")
    #
    
    #if SET_VPULSE_LOW<0 or SET_VPULSE_LOW > 127 or SET_VPULSE_HIGH<0 or SET_VPULSE_HIGH > 127 or SET_VCASN<0 or SET_VCASN > 127 or SET_VCLIP<0 or SET_VCLIP > 127:
    #    print("FORBIDEN VALUE!")
    #    return 0
    print("SET_VPULSE_LOW\t%i" % SET_VPULSE_LOW)
    print("SET_VPULSE_HIGH\t%i" % SET_VPULSE_HIGH)
    print("SET_VCLIP\t%i" % SET_VCLIP)
    print("SET_VCASN\t%i" % SET_VCASN)
    
    m.Write(m.Word("SET_VCASN"),SET_VCASN,False)
    m.Write(m.Word("SET_VCLIP"),SET_VCLIP,False)
    m.Write(m.Word("SET_VPULSE_HIGH"),SET_VPULSE_LOW,False)
    m.Write(m.Word("SET_VPULSE_LOW"),SET_VPULSE_LOW,False)
    
    m.Write(m.Word("SET_VRESET_P"),29,False)
    m.Write(m.Word("SET_VRESET_D"),65,False)
    #'''
    
    #m.Write(m.Word("SET_VCASN"),0,False)
    #m.Write(m.Word("SET_VCLIP"),0,False)
    #m.Write(m.Word("SET_VPLSE_HIGH"),0,False)
    #m.Write(m.Word("SET_VPLSE_LOW"),0,False)
    #m.Write(m.Word("SET_VRESET_P"),0,False)
    #m.Write(m.Word("SET_VRESET_D"),0,False)
    
    # temp encoder
    '''
    
    m.Write(m.Word("SET_ICASN"),5,False)# put them in 1 to test
    m.Write(m.Word("SET_IRESET"),30,False)# put them in 1 to test
    m.Write(m.Word("SET_ITHR"),120,False)# put them in 1 to test
    m.Write(m.Word("SET_IBIAS"),43,False)
    m.Write(m.Word("SET_IDB"),50,False)
    
    #m.Write(m.Word("PULSE_HOR"),0,False)
    
    #m.SetPixelPulseRow(400,True)
    #m.SetPixelPulseColumn(1,True)
    
    for f in range(0,256):
        m.SetDoubleColumnMask(f,True)
        pass
    for dc in range(0, 127):
        m.SetDoubleColumnMask(dc,False)
        pass
    m.SetDoubleColumnMask(170,False)
    #m.SetDoubleColumnMask(164,False)
    m.SetDoubleColumnMask(165,False)
    
    #m.SetDoubleColumnMask(z,False)
    
    ##
    
    
    m.Write(m.Word("SET_IRESET"),0,False)# put them in 1 to test
    m.Write(m.Word("SET_ITHR"),0,False)# put them in 1 to test
    m.Write(m.Word("SET_IBIAS"),0,False)
    m.Write(m.Word("SET_IDB"),0,False)
    m.Write(m.Word("PULSE_HOR"),0,False)
    
    m.Write(m.Word("SWCNTL_DACMONV"),1,False)
    m.Write(m.Word("SWCNTL_DACMONI"),1,False)
    m.Write(m.Word("SET_VPULSE_HIGH"),126,False)
    m.Write(m.Word("SET_ITHR"),30,False)# put them in 1 to test
    #m.Write(m.Word("SET_VCASN"),110,False)# put them in 1 to test
    m.Write(m.Word("SET_ICASN"),10,False)# put them in 1 to test	
    #m.Write(m.Word("SET_VRESETP"),80,False)# put them in 1 to test
    #m.Write(m.Word("SET_IBIAS"),43,False)
    '''
    #SetVPULSE_LOW(110,true)
    #m.SetPixelPulseRow(288,True)
    #m.Write(m.Word("PULSE_COL"),10,False)
    m.SetPixelPulseColumn(x,True)
    m.PrintFullSCWord()
    m.WriteAll()# needed because setdefaults is set to False
    time.sleep(0.01)
    # resets everything on FPGA
    m.SetState(0)
    time.sleep(0.01)
    # start test
    m.SetState(1)
    time.sleep(0.01)
    #m.DumpByWord(True)#DO NOT USE TRUE
    #m.SetState(0)
    return 0# m.CheckSlowControl(False)#DO NOT USE TRUE



if args.RESET==True:
    m.Reset()
else:
    dc=1
    total_errors=0
    for x in range(0,repetitions):
        #time.sleep(0.1)
        total_errors+=test_changing_settings_FIFO(x)
    print("Number of bits that don't match: %i/4321" % total_errors)
    pass

#m.Reset()
print ("Done")
sys.exit()

