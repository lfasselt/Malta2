#!/usr/bin/env python

import os
import argparse

parser=argparse.ArgumentParser()
parser.add_argument("-a","--address",help="connection string udp://host:port",default="udp://ep-ade-gw-02.cern.ch:50008")
parser.add_argument("-c","--chip",help="WXRX",default="W12R19")
parser.add_argument("-C","--config",help="configuration file with dac values",default="configs/Malta2_example.txt")
parser.add_argument("-o","--output",help="describe the measurement",default="test")
parser.add_argument("-r","--repetitions",help="repetitions",default="200")
parser.add_argument("-t","--time",help="time between repetitions (us)",default="500")
#parser.add_argument("-p0","--parMin",help="IDB LOW (5-127)",default="49")
parser.add_argument("-p0","--parMin",help="IDB LOW (5-127)",default="5")
parser.add_argument("-p1","--parMax",help="IDB HIGH (5-127)",default="120")
parser.add_argument("-s","--parStep",help="IDB step",default="1")
parser.add_argument("-n","--npix",help="number of pixels to print",default="30")
parser.add_argument("-d","--dut",help="is a DUT",action="store_true")
parser.add_argument("-v","--verbose",help="turn on verbose mode",action="store_true")
parser.add_argument("-q","--quiet",help="turn on quiet mode",action="store_true")
args=parser.parse_args()

cmd = "Malta2NoiseScan " 
cmd += " -a "+args.address # address
cmd += " -o "+args.chip+"_"+args.output # output
cmd += " -f "+args.chip # basefolder
cmd += " -l "+args.parMin # paramMin
cmd += " -h "+args.parMax # paramMax
cmd += " -s "+args.parStep # paramStep
cmd += " -r "+args.repetitions # repetitions
cmd += " -t "+args.time # time
cmd += " -n "+args.npix # number of pixels
cmd += (" -d" if args.dut else "") # is a dut
cmd += (" -v" if args.verbose else "") # verbose
cmd += (" -q" if args.quiet else "") # quiet
cmd += " -c "+args.config # config file

print(cmd)
os.system(cmd)



