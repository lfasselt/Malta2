#!/usr/bin/env python
##################################################
#
# Monitoring of resistivity measurements with Arduino
#
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# Sascha.Dungs@cern.ch
# July 2016
#
##################################################

import os
import sys
import math
import ROOT
import array
import time
import argparse
import SerialCom
import realtime
import signal
import datetime


class Mon:
    def __init__(self, port='/dev/ttyACM0'):
        V=0
        R=1
        
        chmap={}
        chmap[0]=V
        chmap[1]=V#None
        chmap[2]=V#None
        chmap[3]=V#None
        chmap[4]=V#None
        chmap[5]=V#None
        chmap[6]=V#None
        chmap[7]=R
        chmap[8]=R#None
        chmap[9]=R#None
        chmap[10]=R#None
        chmap[11]=R#None
        chmap[12]=R#None
        chmap[13]=R#None
        chmap[14]=R#None
        chmap[15]=R#None
        self.ser = SerialCom.SerialCom(port, baudrate=9600, terminator="\n")
        pass
    def Read(self, ch_req):
        for x in range(0,15):
            #time.sleep(0.01)
            line = self.ser.read()
            #print("Line: %s" % line)
            l=line.split(",")
            if len(line)==0: continue
            k = l[0]
            #print(line)
            if k=="t":
                #v=int(v)+startTime #not used
                pass
            elif k=="R" or k=="V":
                if len(l)<1:continue
                ch=int(l[1])
                #print("Ch \t[%i]\tR" % ch)
                adc=int(float(l[3]))
                value=float(l[2]) if adc!=0 else 0
                #print(" ch=%i\t%s=%.2f\t Raw ADC val:%2.f \t dt=%s" % (ch,k.upper(),value, adc,now-startTime))
                if ch==ch_req:
                    return value
                pass
            pass
        pass
