#!/usr/bin/env python
"""
runThAnalysisMalta2.py
This runs at the end of threshold scans to make plots.
"""

import os
import sys
import argparse
import ROOT
from math import sqrt


def Selected(sigma, thres, chi2, DoF, h):
  # returns  1 if entry is accepted.
  # returns -1 otherwise.
  selected = False;
  if(DoF>0):
    if (sigma > 3) and (thres < 800) and (chi2/DoF <3) : selected= True;

  if (sigma < 3)    : h.AddBinContent(1,1);
  if (thres > 800)  : h.AddBinContent(2,1);
  if(DoF>0):
    if (chi2/DoF > 3) : h.AddBinContent(3,1);
  
  return selected;

def setStyleTH1(t, x_axis, y_axis="entries", col=1):
  t.SetLineColor(col)
  t.SetLineWidth(2)
  t.GetXaxis().SetTitle(x_axis)
  t.GetYaxis().SetTitle(y_axis)
  t.SetMinimum(0)
  return

def setStyleTH2(t, x_axis, y_axis="entries", col=1):
  t.GetXaxis().SetTitle(x_axis)
  t.GetYaxis().SetTitle(y_axis)
  t.GetZaxis().SetNdivisions(20)
  t.SetContour(100)
  return

def profile2Dhistogram(twoD, x, y):
  average = 0
  nbins = 0
  std=0;

  for i in range(0,twoD.GetNbinsX()) :
    average=0;
    nbins=0;
    std=0;
    for j in range(0,twoD.GetNbinsY()) :
      if (twoD.GetBinContent(i+1,j+1)==0) : continue
      average+=twoD.GetBinContent(i+1,j+1)
      nbins+=1      
      if (nbins != 0) : std+=(average/nbins)-twoD.GetBinContent(i+1,j+1)**2

    if (nbins > 1) :
      x.SetBinContent(i+1, average/nbins);
      if(std/(nbins-1)>0) : x.SetBinError(i+1, sqrt(std/(nbins-1)) );
   
    for i in range(0, twoD.GetNbinsY()) :
      average=0;
      nbins=0;
      std=0;
      for j in range(0, twoD.GetNbinsX()) :
        if (twoD.GetBinContent(j+1,i+1)==0) : continue
        average+=twoD.GetBinContent(j+1,i+1)
        nbins+=1
        if (nbins != 0) : std+= ((average/nbins)-twoD.GetBinContent(j+1,i+1)**2)
      if (nbins > 1) :
        y.SetBinContent(i+1, average/nbins)
        if(std/(nbins-1)>0): y.SetBinError(i+1, sqrt(std/(nbins-1)))

  return

def PrintHistograms(f1,output):
  print("PrintHistograms()")

  c1 = ROOT.TCanvas("c11","c11", 700,600)
  c1.SetRightMargin(0.25)

  title = ""

  for key in f1.GetListOfKeys() :
    if ( ROOT.gROOT.GetClass(key.GetClassName()).InheritsFrom("TH1F") ):
      h = key.ReadObj()
      title = h.GetTitle()
      if (h.GetEntries()==0) : continue
      h.GetXaxis().SetRangeUser(h.GetMinimum(),h.GetMaximum())
      title = h.GetName()
      if ("profile" in title  ) :
        h.Draw("ep") 
      else :
        # VD: min/max protection
        Max=0
        Min=0
        for bin in range(1, h.GetNbinsX()) :
          if (h.GetBinContent(bin)!=0) :
            Min=h.GetXaxis().GetBinCenter(bin)-10
            break
        for bin in range(h.GetNbinsX(),0) :
          if (h.GetBinContent(bin)!=0) :
            Max=h.GetXaxis().GetBinCenter(bin)+10
            break
        h.GetXaxis().SetRangeUser(Min,Max)
        h.Draw()
        h.GetXaxis().SetRangeUser(Min,Max)

      ROOT.gPad.Update()
      c1.Print((output+"/"+title+".pdf"))
      if ( ("profile" in title)  and ("noise" in title) ) : 
        h.SetMinimum(0.5)
        c1.SetLogy()
        h.Draw()
        ROOT.gPad.Update()
        c1.Print((output+"/"+title+"_log.pdf"))
      c1.SetLogy(0)

    if (ROOT.gROOT.GetClass(key.GetClassName()).InheritsFrom("TH2F")) :
      h = key.ReadObj()
      title = h.GetTitle()
      if (h.GetEntries()>0) :
        h.Draw("COLZ")
        binmin = int(h.GetXaxis().GetXmin())
        title = h.GetName()
        ROOT.gPad.Update()
        pass
      pass
    c1.Print((output+"/"+title+".pdf"))
    pass
  return

if __name__ == "__main__":


  print("Welcome to runThAnalysisMalta2.py!")

  parser = argparse.ArgumentParser()
  parser.add_argument("-f","--folder", type=str, default=''	, help="specify folder e.g. './ThScansMalta/W7R12/blah'")                      
  parser.add_argument("-e","--even", type=bool , default=False	, help="specify if the scan was done only for even pixels")                      
  args = parser.parse_args()
  
  ROOT.gROOT.SetBatch(True)
  ROOT.gStyle.SetPalette(112)

  print("Processing: %s" % args.folder)

  if not os.path.exists(args.folder):
    print ("Input folder DOES NOT EXIST .... ABORTING")
    sys.exit()

  FileTree = ROOT.TFile.Open(args.folder+"/th_sum_IDB_.root")
  Tree = FileTree.Thres
  
  min_pix_X = 0
  max_pix_X = 512
  min_pix_Y = 0
  max_pix_Y = 512
  
  g_threshold = ROOT.TF1("g_threshold","gaus", 200,500)
  g_threshold.SetParameters(1,250,50)
  
  if( (max_pix_Y<min_pix_Y) or (max_pix_X<min_pix_X) ):
    raise Exception("There's a problem with the pixel ranges!")
    
  nentries = Tree.GetEntries()
  print("There are "+str(nentries)+" entries in the TTree.")
  
  nbins_2D_x = 512 # max_pix_X-min_pix_X+1
  nbins_2D_y = 512 # max_pix_Y-min_pix_Y+1
  
  output = ROOT.TFile.Open(args.folder+"/plots.root", "RECREATE")
  output.cd()
  
  m_noise_2D = ROOT.TH2F ("noise_2D", "noise_2D", 
                          nbins_2D_x, min_pix_X, max_pix_X,
                          nbins_2D_y,  min_pix_Y, max_pix_Y )
  setStyleTH2(m_noise_2D, "pixX", "pixY")
  m_thres_2D = ROOT.TH2F ("thres_2D", "thres_2D", 
                        nbins_2D_x, min_pix_X, max_pix_X,
                          nbins_2D_y,  min_pix_Y, max_pix_Y )
  setStyleTH2(m_thres_2D, "pixX", "pixY")
  
  m_thres_pixX_2D = ROOT.TH2F ("thres_pixX_2D", "thres_pixX_2D", 
                               nbins_2D_x, min_pix_X, max_pix_X,
                               20, Tree.GetMinimum("Vh_Vl"), Tree.GetMaximum("Vh_Vl") )
  setStyleTH2(m_thres_pixX_2D, "pixX", "threshold (e)")
  
  m_thres_2D_pixX = ROOT.TH1F ("thres_2D_pixX_profile", "thres_2D_pixX_profile", 
                               nbins_2D_x, min_pix_X,  max_pix_X+1 )
  setStyleTH1(m_thres_2D_pixX,"pix X", "threshold (e)")
  m_thres_2D_pixY = ROOT.TH1F ("thres_2D_pixY_profile", "thres_2D_pixY_profile",
                               nbins_2D_y, min_pix_Y,  max_pix_Y+1 )
  setStyleTH1(m_thres_2D_pixY,"pix Y", "threshold (e)")
  
  m_noise_2D_pixX = ROOT.TH1F ("noise_2D_pixX_profile", "noise_2D_pixX_profile", 
                               nbins_2D_x, min_pix_X,  max_pix_X+1 )
  setStyleTH1(m_noise_2D_pixX,"pix X", "threshold (e)")
  m_noise_2D_pixY = ROOT.TH1F ("noise_2D_pixY_profile", "noise_2D_pixY_profile", 
                               nbins_2D_y, min_pix_Y,  max_pix_Y+1 )
  setStyleTH1(m_noise_2D_pixY,"pix Y", "threshold (e)")
  
  th_bW=5
  th_min=int((Tree.GetMinimum("Vh_Vl")-15)/th_bW)
  th_max=int((Tree.GetMaximum("Vh_Vl")+15)/th_bW)
  bin_1d=(th_max-th_min)
  
  print(bin_1d, th_min*th_bW, th_max*th_bW)
  
  m_thres_1D = ROOT.TH1F ("thres_1D", "thres_1D", bin_1d, th_min*th_bW, th_max*th_bW)
  setStyleTH1(m_thres_1D,"threshold (e)")
  
  no_bW=0.2
  no_min=int(3/no_bW)
  no_max=int((Tree.GetMaximum("sigma")+0.6)/no_bW)
  bin_1d=(no_max-no_min)
  m_noise_1D = ROOT.TH1F ("noise_1D", "noise_1D", bin_1d, no_min*no_bW, no_max*no_bW)
  setStyleTH1(m_noise_1D,"noise (e)")
  
  m_pix_not_sel= ROOT.TH1F("pix_not_sel", "pix_not_sel", 3, 0, 1 )
  setStyleTH1(m_pix_not_sel,"")
  m_pix_not_sel.GetXaxis().SetBinLabel(1,"sigma > 4")
  m_pix_not_sel.GetXaxis().SetBinLabel(2,"thres < 800")
  m_pix_not_sel.GetXaxis().SetBinLabel(3,"chi2/DoF < 3")
  
  m_thres_1D_array=[]
  for i in range( 0, 32) :
    m_thres_1D_array.append(ROOT.TH1F("thres_1D_DACgroup_"+str(i),
                                      "thres_1D_DACgroup_"+str(i),
                                      int(sqrt(nentries)), Tree.GetMinimum("Vh_Vl"), Tree.GetMaximum("Vh_Vl") ) )
    setStyleTH1(m_thres_1D_array[i],"threshold (e)","entries", i+1)       
    
  m_thres_1D_sector_array=[]
  m_DACGroups_1D_perSectorArray=[]
  for i in range(0,8) :
    m_thres_1D_sector_array.append( ROOT.TH1F ("thres_1D_sector_"+str(i),
                                               "thres_1D_sector_"+str(i), 
                                               int(sqrt(nentries)),
                                               Tree.GetMinimum("Vh_Vl"),
                                               Tree.GetMaximum("Vh_Vl") ) )
    setStyleTH1(m_thres_1D_sector_array[i],"threshold (e)","entries", i+1)
    
    m_DACGroups_1D_perSectorArray.append( ROOT.TH1F ("m_DACGroups_1D_perSectorArray"+str(i),
                                                     "m_DACGroups_1D_perSectorArray"+str(i),
                                                     int(sqrt(nentries)),
                                                     Tree.GetMinimum("Vh_Vl"),
                                                     Tree.GetMaximum("Vh_Vl") ) )
    setStyleTH1(m_DACGroups_1D_perSectorArray[i],"threshold (e)","entries", i+1)
    
  nbytes = 0
  nb = 0

  for jentry in range(0,nentries):      
    nb = Tree.GetEntry(jentry)   
    nbytes += nb
    if (Selected(Tree.sigma, Tree.thres, Tree.chi2, Tree.DoF, m_pix_not_sel)==False) : continue
        
    m_thres_1D.Fill(Tree.thres)
    m_noise_1D.Fill(Tree.sigma)
    m_thres_2D.Fill(Tree.pixX,Tree.pixY,Tree.thres)
    m_noise_2D.Fill(Tree.pixX,Tree.pixY,Tree.sigma)
    m_thres_pixX_2D.Fill(Tree.pixX,Tree.thres)
    m_thres_1D_sector_array[int(Tree.pixX/64)].Fill(Tree.thres)
    m_thres_1D_array[int(Tree.pixX/16)].Fill(Tree.thres)
    pass  
  
  m_DACGroups_1D = ROOT.TH1F("thres_1D_meanDACgroup", 
                           "thres_1D_meanDACgroup",
                           int(sqrt(nentries)),
                           Tree.GetMinimum("Vh_Vl"),
                           Tree.GetMaximum("Vh_Vl") )
    
  for i in range(0, 32) :
    #print("i: "+str(i))
    m_DACGroups_1D.Fill(m_thres_1D_array[i].GetMean())
    m_DACGroups_1D_perSectorArray[int(i/4)].Fill(m_thres_1D_array[i].GetMean())
    pass
  
  m_noise_2D.GetZaxis().SetRangeUser(Tree.GetMinimum("sigma"), Tree.GetMaximum("sigma"))
  m_thres_2D.GetZaxis().SetRangeUser(Tree.GetMinimum("Vh_Vl"), 500)

  m_thres_1D.Fit("g_threshold")

  print("Writing output.")
  output.Write()
  PrintHistograms(output,args.folder)
  output.Close()
  
  print("All done!")
  
  #del Tree
  #del FileTree
