#!/usr/bin/env python
import os
import argparse

class ArgumentParserWithDefaults(argparse.ArgumentParser):
    def add_argument(self, *args, help=None, default=None, **kwargs):
        if help is not None:
            kwargs['help'] = help
        if default is not None and args[0] != '-h':
            kwargs['default'] = default
            if help is not None:
                kwargs['help'] += ' Default: {}'.format(default)
        super().add_argument(*args, **kwargs)

#parser=argparse.ArgumentParser()

parser = ArgumentParserWithDefaults(
    formatter_class=argparse.RawTextHelpFormatter
)

parser.add_argument("-a" ,"--address" ,help="connection string udp://host:port" ,default="udp://ep-ade-gw-07.cern.ch:60000", required=True)
parser.add_argument("-c" ,"--chip"    ,help="WXRX"                              ,default="W12R19", required=True)
parser.add_argument("-T" ,"--tag"     ,help="describe the measurement"          ,default="TEST")
parser.add_argument("-C" ,"--config"  ,help="configuration file with dac values",default="configs/Malta2_example.txt")
parser.add_argument("-t" ,"--triggers",help="number of triggers"                ,default="160")
parser.add_argument("-p0","--parMin"  ,help="VLOW (5-127)"                      ,default="20")
parser.add_argument("-p1","--parMax"  ,help="VHIGH (5-127)"                     ,default="89")
#parser.add_argument("-r" ,"--region",help="a b c dregion [xmin(0-511) No.x ymin(>287) No.y (0-223)]") ## NOT USED
parser.add_argument("-x0","--col0"    ,help="first column [0-511]"              ,default="0")
parser.add_argument("-nx","--ncols"   ,help="number of columns [1-512]"         ,default="512")
parser.add_argument("-y0","--row0"    ,help="first row [288-511]",default="288")
parser.add_argument("-ny","--nrows"   ,help="first column [1-224]",default="224")

parser.add_argument("-s" ,"--parStep",help="Parameter step",default="1")

parser.add_argument("-e" ,"--evenonly",help="only scans event pixels",action="store_true")
parser.add_argument("-v" ,"--verbose" ,help="turn on verbose mode"   ,action="store_true")

args=parser.parse_args()

cmd = "Malta2ThresholdScan " 
cmd += " -a "+args.address # address
cmd += " -o "+args.chip+"_"+args.tag # output
cmd += " -f "+args.chip # basefolder
cmd += " -l "+args.parMin # paramMin
cmd += " -h "+args.parMax # paramMax
cmd += " -s "+args.parStep # paramStep
cmd += " -r %s %s %s %s" % (args.col0,args.ncols,args.row0,args.nrows) # region [xmin(0-511) ymin(>287) No.y (0-223)] ### a b c d : starting pixel (a c), pulsing b cols and d rows
cmd += " -t "+args.triggers # trigger
cmd += " -q" # quiet 
cmd += " -F" # fast scan  ## VD this is dangerous!!!!
cmd += (" -e" if args.evenonly else "") # verbose
cmd += (" -v" if args.verbose else "")  # verbose
cmd += " -c "+args.config # config file

print(cmd)
os.system(cmd)
