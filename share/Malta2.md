# Malta2 {#PackageMalta2}

Malta2 is the software for MALTA2. It requires MaltaDAQ.

## Applications

- Malta2AnalogScan: Application to perform an analog scan on MALTA2. Inject a known charge to a range of pixels.
- Malta2ThresholdScan: Application to perform a threshold scan on MALTA2. Inject an increasing value of charge to a range of pixels, fit an s-curve, and extract the threshold and noise from it.
- Malta2TapCalibration: Application to perform a tap calibration. Change the values of the taps for the firmware and produce a plot that shows the position of the hits vs the tap value.
- Malta2NoiseScan: Application to perform a noise scan. Iteratively run random triggers as a function of IDB until the noisy pixels have been identified and/or masked.

## Libraries

- Malta2: Library containing DAQ classes for MALTA2

## Malta2 library

- Malta2: Control MALTA2
- Malta2Data: description of the data of MALTA2
- Malta2Tree: write data to root files
- Malta2Module: class to run MALTA2 as a plane in MaltaMultiDAQ
- Malta2Utils: helper tools for different scans
