#!/usr/bin/env python3
import os
import sys
import argparse
import PyMalta2


#Test declaration of arguments needed to run it
parser = argparse.ArgumentParser()
parser.add_argument("-c","--constr", help='connection string (udp://host:port)',required=True)
args = parser.parse_args()

malta2=PyMalta2.Malta2()
malta2.Connect(args.connstr)
malta2.SetVDS_VBCMFB(0x7) # 4-bit encoded pulse length, we could try all 16 combinations  
malta2.Send()
malta2.Trigger(1000,True) # number of times, pulse or not
	

