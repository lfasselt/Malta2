############################
# Malta2
# Carlos.Solans@cern.ch
# Ignacio.Asensi@cern.ch
############################

tdaq_package()

find_package(ROOT COMPONENTS Matrix Tree Hist GenVector Gpad RIO Graf)

tdaq_add_scripts(*.py)
tdaq_add_scripts(share/*.py)
tdaq_add_python_files(python/*.py)

tdaq_add_library(Malta2 
                 src/Malta2.cpp
		 src/Malta2Data.cpp
		 src/Malta2Tree.cpp
		 src/Malta2Module.cpp
		 src/Malta2Utils.cpp
		 src/Malta2Masker.cpp
		 LINK_LIBRARIES ROOT ipbus ROOT::Hist MaltaDAQ)

tdaq_add_library(PyMalta2
                 src/PyMalta2.cxx
                 INCLUDE_DIRECTORIES ${PYTHON_INCLUDE_DIR}
                 OPTIONS -Wno-register
                 LINK_LIBRARIES ipbus Malta2
                 NOINSTALL
                 )

install(FILES ${CMAKE_CURRENT_BINARY_DIR}/libPyMalta2.so 
        DESTINATION ${BINARY_TAG}/lib 
        RENAME PyMalta2.so) 

tdaq_add_executable(Malta2AnalogScan 
                    src/Malta2AnalogScan.cpp
		    LINK_LIBRARIES Malta2 ROOT ipbus ROOT::Hist MaltaDAQ tdaq::cmdline)

tdaq_add_executable(Malta2ThresholdScan
                    src/Malta2ThresholdScan.cpp
 		    src/Malta2ThresholdAnalysis.cpp
                    LINK_LIBRARIES Malta2 ROOT ipbus ROOT::Hist MaltaDAQ tdaq::cmdline)

tdaq_add_executable(Malta2ThresholdScan_jw
                    src/Malta2ThresholdScan_jw.cpp
 		    src/Malta2ThresholdAnalysis.cpp
                    LINK_LIBRARIES Malta2 ROOT ipbus ROOT::Hist MaltaDAQ tdaq::cmdline)

tdaq_add_executable(Malta2TapCalibration
                    src/Malta2TapCalibration.cpp
		    LINK_LIBRARIES Malta2 ROOT ipbus ROOT::Hist MaltaDAQ tdaq::cmdline)

tdaq_add_executable(Malta2NoiseScan
                    src/Malta2NoiseScan.cpp
		    LINK_LIBRARIES Malta2 ROOT ipbus ROOT::Hist MaltaDAQ tdaq::cmdline)

tdaq_add_executable(Malta2ThresholdScanNew
                    src/Malta2ThresholdScanNew.cpp
		    LINK_LIBRARIES Malta2 ROOT ipbus ROOT::Hist MaltaDAQ tdaq::cmdline)

tdaq_add_executable(Malta2AnalogDacScan 
                    src/Malta2AnalogDacScan.cpp
		    LINK_LIBRARIES Malta2 ROOT ipbus ROOT::Hist MaltaDAQ tdaq::cmdline)

tdaq_add_data(share/Malta2.md)
