//! -*-C++-*-

// MALTA2
#include "Malta2/Malta2.h"
#include "Malta2/Malta2Data.h"
#include "Malta2/Malta2Tree.h"
#include "Malta2/Malta2Utils.h"
#include "Malta2/Malta2Masker.h"

#include <cmdl/cmdargs.h>

// C++ includes
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <signal.h>
#include <iomanip>
#include <chrono>
#include <time.h>
#include <algorithm>
#include <sys/stat.h>

#include "TFile.h"
#include "TH1F.h"
#include "TF1.h"
#include "TGraph.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TROOT.h"

using namespace std;

bool g_cont=true;

void handler(int){
  cout << "You pressed ctrl+c to quit" << endl;
  g_cont=false;
}

int main(int argc, char *argv[]){
  
  cout << "##############################################" << endl
       << "# Welcome to the new MALTA2 Analog DAC Scan  #" << endl
       << "##############################################" << endl;
  
  CmdArgBool    cVerbose( 'v',"verbose","turn on verbose mode");
  CmdArgBool    cQuiet(   'q',"quiet","do not write root files");
  CmdArgBool    cNoHists( 'Q',"nohists","do not write histograms");
  CmdArgStr     cAddress( 'a',"address","address","connection string. udp://host:port",CmdArg::isREQ);
  CmdArgStr     cChip(    'c',"chip","sample","WXRX. Required",CmdArg::isREQ);
  CmdArgStr     cTag(     't',"tag","text","test description. Default test");
  CmdArgStr     cDac(     'd',"dac","name","DAC to scan (IDB,ITHR). Default IDB");
  CmdArgInt     cParamMin('m',"paramMin","paramMin","Param low (5-127). Default 5");
  CmdArgInt     cParamMax('M',"paramMax","paramMax","Param high (5-127). Default 127");
  CmdArgInt     cParamStp('s',"paramStp","paramStp","Param step. Default 1");
  CmdArgInt     cVPulseLo('l',"vlo","integer","VPULSE_LOW value [5-127]. Default 10");
  CmdArgInt     cVPulseHi('h',"vhi","integer","VPULSE_HIGH value [5-127]. Default 70");
  CmdArgInt     cCol0(    'x',"col0", "number", "first column [0-511]. Default 0");
  CmdArgInt     cColN(    'X',"colN", "number", "last column [0-511]. Default 511");
  CmdArgInt     cRow0(    'y',"row0", "number", "first row [288-511]. Default 288");
  CmdArgInt     cRowN(    'Y',"rowN", "number", "last row [288-511]. Default 511"); 
  CmdArgInt     cNumTrigs('n',"numtrigs","trigger","the number of triggers. Default 100");
  CmdArgStr     cConfig(  'C',"config","config","configuration file");

  cCol0=0;
  cColN=511;
  cRow0=288;
  cRowN=511;
  cParamMin=5;
  cParamMax=127;
  cParamStp=1;
  cVPulseLo=10;
  cVPulseHi=70;
  cNumTrigs=100;
  
  CmdLine cmdl(*argv,
	       &cVerbose,
	       &cAddress,
	       &cChip,
	       &cTag,
	       &cDac,
	       &cParamMin,
	       &cParamMax,
	       &cParamStp,
	       &cVPulseLo,
	       &cVPulseHi,
	       &cCol0,
	       &cColN,
	       &cRow0,
	       &cRowN,
	       &cNumTrigs,
	       &cQuiet,
	       &cNoHists,
	       &cConfig,
	       NULL);

  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  //find the actual DAC
  uint32_t dIDB=0;
  uint32_t dITHR=1;
  uint32_t iDac=dIDB;
  string sDac=(cDac.flags() & CmdArg::GIVEN?string(cDac):"IDB");
  if     (sDac=="IDB"){  iDac=dIDB;  cout << "Scanning IDB" << endl; } 
  else if(sDac=="ITHR"){ iDac=dITHR; cout << "Scanning ITHR" << endl; }
  else{ cout << "DAC not supported: " << sDac << endl; return 0; }

  //making output directory
  string outdir=getenv("MALTA_DATA_PATH");
  mkdir(outdir.c_str(), S_IFDIR| S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  outdir+="/Malta2";
  mkdir(outdir.c_str(), S_IFDIR| S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  outdir+="/Results_AnalogDacScan";
  mkdir(outdir.c_str(), S_IFDIR| S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  outdir+="/"+string(cChip);
  mkdir(outdir.c_str(), S_IFDIR| S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  outdir+="/"+(cTag.flags() & CmdArg::GIVEN?string(cTag):"test");
  mkdir(outdir.c_str(), S_IFDIR| S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  if(!cNoHists) mkdir((outdir+"/hists").c_str(), S_IFDIR| S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

  signal(SIGINT,handler);
  
  //Connect to MALTA2  
  Malta2 * malta = new Malta2();
  malta->Connect(string(cAddress));

  malta->SetVerbose(false);
  malta->WriteConstDelays(4,1);
  malta->SetExternalL1A(false);
  malta->SetReadoutDelay(80);

  cout << "#####################################" << endl
       << "# Configuring chip                  #" << endl
       << "#####################################" << endl;
  
  Malta2Utils::PreConfigureMalta(malta);
  Malta2Utils::EnableAll(malta);
  
  if (cConfig.flags() & CmdArg::GIVEN){
    if(!malta->SetConfigFromFile(string(cConfig))){
      cout << "## Cannot parse config file: " << string(cConfig) << endl;
      cout << "## Exit"<<endl;
      delete malta;
      exit(0);
    }
  }else{
    cout << "## Using default config from Malta2Utils" << endl;
  }

  malta->SetVPULSE_LOW(cVPulseLo);
  malta->SetVPULSE_HIGH(cVPulseHi);

  Malta2Utils::MaltaSend(malta);

  //Create histograms and calibration results tree

  uint32_t numParamSteps=(cParamMax-cParamMin)/cParamStp;
  
  TH1F * histos[512][512];    
  for(uint32_t x=0;x<512;x++){
    for(uint32_t y=0;y<512;y++){
      ostringstream os;
      os << "HitsVsParam"
	 << "_" << setw(3) << setfill('0') << x
	 << "_" << setw(3) << setfill('0') << y;
      histos[x][y]=new TH1F(os.str().c_str(),";DAC;Entries",numParamSteps+1,cParamMin,cParamMax+1);
      histos[x][y]->SetDirectory(0);
    }
  }
  
  //Build the masker
  Malta2Masker masker;
  masker.SetVerbose(cVerbose);
  masker.SetRange(cCol0,cRow0,cColN+1,cRowN+1);
  masker.SetShape(2,8);
  masker.SetFrequency(1);
  masker.Build();
  
  //Create ntuple
  Malta2Tree *ntuple = new Malta2Tree();
  if (!cQuiet){
    ntuple->Open((outdir+"/scan_data.root").c_str(),"RECREATE");
    ntuple->Extend();
    //ntuple->SetIDB(...);
    ntuple->SetVHIGH(cVPulseHi);
    ntuple->SetVLOW(cVPulseLo);
  }

  cout << "#####################################" << endl
       << "# Start Mask loop                   #" << endl
       << "#####################################" << endl;
  
  //all pixel scanning clock start
  time_t t_scan_0;
  time(&t_scan_0);
  signal(SIGINT,handler);
  int pixels=0;

  for(uint32_t step=0; step<masker.GetNumSteps(); step++){
    if(!g_cont) break;

    time_t t_step_0;
    time(&t_step_0);
    
    cout << "Step " << (step+1) << "/" << masker.GetNumSteps() << endl;

    masker.GetStep(step);
    if(!cQuiet){ ntuple->SetL1idC(step); }
      
    //Unmask desired pixel
      
    for (uint32_t r=0; r<512; r++) malta->SetPixelPulseRow(r,false);
    for (uint32_t c=0; c<512; c++) malta->SetPixelPulseColumn(c,false);
    for (uint32_t a=0; a<256; a++) malta->SetDoubleColumnMask(a,true);

    for(pair<uint32_t,uint32_t> pix : masker.GetPixels()){
      pixels+=1;
      malta->SetPixelPulseColumn(pix.first,true);
      malta->SetPixelPulseRow(pix.second,true);
    }

    for(uint32_t dc : masker.GetDoubleColumns()){
      malta->SetDoubleColumnMask(dc,false);
    }
    
    for(uint32_t paramVal=cParamMin;paramVal<=cParamMax;paramVal+=cParamStp){
      
      if(!g_cont) break;

      cout << "Param: " << paramVal << endl;
      
      //Change the DAC
      if(iDac==dIDB){
	malta->SetIDB(paramVal);
	if(!cQuiet) ntuple->SetIDB(paramVal);
      }
      else if(iDac==dITHR){
	malta->SetITHR(paramVal);
	if(!cQuiet) ntuple->SetITHR(paramVal);
      }      

      //actually configure malta2
      Malta2Utils::MaltaSend(malta);
    
      //Flush MALTA
      malta->ResetFifo();
      malta->ReadoutOn();
      malta->Trigger(cNumTrigs,true);
      malta->ReadoutOff();
      
      Malta2Data md,md2;
      int maxwords=200;
      uint32_t words[200];
      for (uint32_t i=0;i<maxwords;i++){ words[i]=0;}
    
      bool isFirst=true;
      int numberOfHits=0;     

      //Readout 
      while(g_cont){	      
	int numwords = malta->GetFIFO2WordCount();
	if(numwords > maxwords){ numwords = maxwords; }
	
	malta->ReadMaltaWord(words,numwords);
	
	// This is the way out of the loop.
	if (words[0]==0){
	  break;
	}
	
	for (uint32_t i = 0; i < numwords; i+=2){		      
	  if (isFirst){
	    md.setWord1(words[0]);
	    md.setWord2(words[1]);
	    isFirst = false;
	    continue;
	  }
	  
	  md2.setWord1(md.getWord1());
	  md2.setWord2(md.getWord2());
	  md.setWord1(words[0+i]);
	  md.setWord2(words[1+i]);
	  md.unPack();
	  md2.unPack();
	  
	  bool markDuplicate = Malta2Utils::markDuplicate(md2.getBcid(),md.getBcid(),
							  md2.getWinid(),md.getWinid(),
							  md2.getL1id(),md.getL1id(),
							  md2.getPhase(),md.getPhase());
	  
	  if (!cQuiet){
	    ntuple->Set(&md2);
	    ntuple->SetIsDuplicate(markDuplicate?1:0);
	    ntuple->Fill();
	  }
	  if (markDuplicate==0 ){ 
	    for (unsigned h=0; h<md2.getNhits(); h++){
	      uint32_t pixX=md2.getHitColumn(h);
	      uint32_t pixY=md2.getHitRow(h);
	      if(cVerbose) cout << "hit: " << pixX << "," << pixY << endl;
	      if (masker.Contains(pixX,pixY)){ 
		numberOfHits++;
		histos[pixX][pixY]->Fill(paramVal);
	      }
	    }
	  }
	}
      }
      cout << "Number of hits in step: " << numberOfHits << endl;
    }

    time_t t_step_1;
    time(&t_step_1);
    uint32_t dtime = (t_step_1-t_step_0);
    cout << "Elapsed time of step: " << dtime  << " seconds" << endl;
  }

  //Cleaning the house
  if(!cQuiet) ntuple->Close();
  
  delete ntuple;
  delete malta;

  cout << "#####################################" << endl
       << "# Analyze the data                  #" << endl
       << "#####################################" << endl;

  TFile * tout = new TFile((outdir+"/scan_results.root").c_str(),"RECREATE");
  /*
  TTree * t_tree = new TTree("dac_scan","dac scan data");
  uint32_t pixX;
  uint32_t pixY;
  float dof;
  float chi2;
  float scale;
  float thres;
  float sigma;
  t_tree->Branch("pixX", &pixX,    "pixX/I");
  t_tree->Branch("pixY", &pixY,    "pixY/I");
  t_tree->Branch("dof",  &dof,     "DoF/i");
  t_tree->Branch("chi2", &chi2,    "chi2/f");
  t_tree->Branch("scale",&scale,   "Scale/f");
  t_tree->Branch("thres",&thres,   "VH_VL/f");
  t_tree->Branch("sigma",&sigma,   "sigma/f"); 
  */

  TH2F * hsumm = new TH2F("HitsVsParam",";Param;Entries",numParamSteps+1,cParamMin,cParamMax+1,cNumTrigs, 0, cNumTrigs);
  //TF1 * scurve = new TF1("scurve","[0]/2*(1+TMath::Erf(1/TMath::Sqrt(2)*(x-[1])/[2]))");
  //TH1F * hThres = new TH1F("Threshold",";threshold [VH-VL]",numParamSteps*10,0,cParamMax-cParamMin+5);
  //TCanvas * can = new TCanvas("can","can",800,600);  

  for(uint32_t x=0;x<512;x++){
    for(uint32_t y=0;y<512;y++){
      if(histos[x][y]->GetEntries()==0) continue;
      if(!g_cont){break;}

      cout << "histo[" << x << "][" << y << "]:";
      for(uint32_t i=0;i<histos[x][y]->GetNbinsX();i++){
	cout << setw(4) << histos[x][y]->GetBinContent(i+1) << ",";
      }
      cout << endl;
      for(uint32_t i=0;i<histos[x][y]->GetNbinsX();i++){
	hsumm->Fill(histos[x][y]->GetBinCenter(i+1),histos[x][y]->GetBinContent(i+1)); 
      }
      /*
      scurve->SetParameter(0,cNumTrigs);
      scurve->SetParLimits(0,0.5*cNumTrigs,1.5*cNumTrigs);
      scurve->SetParameter(1,(cParamMax-cParamMin)/2);
      scurve->SetParLimits(1,0,(cParamMax-cParamMin));
      scurve->SetParameter(2,10);
      scurve->SetParLimits(2,0.1,30);
      histos[x][y]->Fit(scurve,"Q");
      pixX=x;
      pixY=y;
      dof=scurve->GetNDF();
      chi2=scurve->GetChisquare();
      scale=scurve->GetParameter(0);
      thres=scurve->GetParameter(1);
      sigma=scurve->GetParameter(2);
      t_tree->Fill();
      if(scurve->GetChisquare()/scurve->GetNDF()<5){ hThres->Fill(thres); }
      histos[x][y]->Write();
      if(cNoHists) continue;
      ostringstream os;
      os << outdir << "/hists/histo"
	 << "_" << setw(3) << setfill('0') << pixX
	 << "_" << setw(3) << setfill('0') << pixY
	 << ".pdf";
      histos[x][y]->Draw();
      can->Print(os.str().c_str());
      */
    }
  }

  /*
  TF1 * gaus = new TF1("gaus","gaus(x)");
  gaus->SetParameter(0,pixels/2);
  gaus->SetParLimits(0,1,pixels);
  gaus->SetParameter(1,(cParamMax-cParamMin)/2);
  gaus->SetParLimits(1,0,(cParamMax-cParamMin));
  gaus->SetParameter(2,1);
  gaus->SetParLimits(2,0.01,30);
  
  hThres->Fit(gaus,"Q");
  cout << "Chi2/Ndof: " << gaus->GetChisquare()/gaus->GetNDF() << endl
       << "Chi2:  " << gaus->GetChisquare() << endl
       << "Ndof:  " << gaus->GetNDF() << endl
       << "Scale: " << gaus->GetParameter(0) << endl
       << "Thres: " << gaus->GetParameter(1) << endl
       << "Noise: " << gaus->GetParameter(2) << endl;
  if(gaus->GetChisquare()/gaus->GetNDF()>5){
  cout << "Threshold fit did not converge" << endl;
  }
  
  can->cd();
  hThres->Draw();
  can->Print((outdir+"/threshold.pdf").c_str());
  delete can;
  */
  
  hsumm->Write();
  //t_tree->Write();
  //hThres->Write();


  
  delete tout;
    
  cout << "delete histos" << endl;
  for(uint32_t x=0;x<512;x++){
    for(uint32_t y=0;y<512;y++){
      delete histos[x][y];
    }
  }
   
  time_t t_scan_1;
  time(&t_scan_1);
  float scan_time = t_scan_1 - t_scan_0;
  cout << "TOTAL scantime: " << scan_time << " seconds for: " << pixels << "  pixels " << endl;
  cout << "Check results in : " << outdir << endl;

  cout << "Have a nice day!" << endl;
  return 0;
}
