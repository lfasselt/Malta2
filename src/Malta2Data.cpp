#include "Malta2/Malta2Data.h"
#include <sstream>
#include <iostream>
#include <bitset>

using namespace std;

Malta2Data::Malta2Data(){
  m_refbit = 0;
  m_pixel = 0;
  m_group = 0;
  m_parity = 0;
  m_delay = 0;
  m_dcolumn = 0;
  m_chipbcid = 0;
  m_chipid = 0;
  m_phase = 0;
  m_winid = 0;
  m_l1id = 0;
  m_bcid = 0;
  m_word1 = 0;
  m_word2 = 0;
  m_rows.resize(16);
  m_columns.resize(16);
}

Malta2Data::~Malta2Data(){}

void Malta2Data::setRefbit(uint32_t value){
  m_refbit = value;
}

void Malta2Data::setPixel(uint32_t value){
  m_pixel = value;
}

void Malta2Data::setGroup(uint32_t value){
  m_group = value;
}

void Malta2Data::setParity(uint32_t value){
  m_parity = value;
}

void Malta2Data::setDelay(uint32_t value){
  m_delay = value;
}

void Malta2Data::setDcolumn(uint32_t value){
  m_dcolumn = value;
}

void Malta2Data::setChipbcid(uint32_t value){
  m_chipbcid = value;
}

void Malta2Data::setChipid(uint32_t value){
  m_chipid = value;
}

void Malta2Data::setPhase(uint32_t value){
  m_phase = value;
}

void Malta2Data::setWinid(uint32_t value){
  m_winid = value;
}

void Malta2Data::setL1id(uint32_t value){
  m_l1id = value;
}

void Malta2Data::setBcid(uint32_t value){
  m_bcid = value;
}

void Malta2Data::setWord1(uint32_t value){
  m_word1 = value;
}

void Malta2Data::setWord2(uint32_t value){
  m_word2 = value;
}

uint32_t Malta2Data::getRefbit(){
  return m_refbit;
}

uint32_t Malta2Data::getPixel(){
  return m_pixel;
}

uint32_t Malta2Data::getGroup(){
  return m_group;
}

uint32_t Malta2Data::getParity(){
  return m_parity;
}

uint32_t Malta2Data::getDelay(){
  return m_delay;
}

uint32_t Malta2Data::getDcolumn(){
  return m_dcolumn;
}

uint32_t Malta2Data::getChipbcid(){
  return m_chipbcid;
}

uint32_t Malta2Data::getChipid(){
  return m_chipid;
}

uint32_t Malta2Data::getPhase(){
  return m_phase;
}

uint32_t Malta2Data::getWinid(){
  return m_winid;
}

uint32_t Malta2Data::getNhits(){
  return m_nhits;
}

uint32_t Malta2Data::getBcid(){
  return m_bcid;
}

uint32_t Malta2Data::getL1id(){
  return m_l1id;
}

uint32_t Malta2Data::getWord1(){
  return m_word1;
}

uint32_t Malta2Data::getWord2(){
  return m_word2;
}

uint32_t Malta2Data::getHitRow(uint32_t hit){
  return m_rows[hit];
}

uint32_t Malta2Data::getHitColumn(uint32_t hit){
  return m_columns[hit];
}

void Malta2Data::pack(){

  m_word1=0;
  m_word2=0;
  m_word1 |= (m_refbit   & 0x1)    << 0;
  m_word1 |= (m_pixel    & 0xFFFF) << 1;
  m_word1 |= (m_group    & 0x1F)   << 17;
  m_word1 |= (m_parity   & 0x1)    << 22;
  m_word1 |= (m_delay    & 0x7)    << 23;
  m_word1 |= (m_dcolumn  & 0x1F)   << 26;
  m_word2 |= (m_dcolumn  >> 5) & 0x7;
  m_word2 |= (m_chipbcid & 0x3)    << (34-31);
  m_word2 |= (m_chipid   & 0x1)    << (36-31);
  m_word2 |= (m_phase    & 0x7)    << (37-31);
  m_word2 |= (m_winid    & 0xF)    << (40-31);
  m_word2 |= (m_bcid     & 0x3F)   << (44-31);
  m_word2 |= (m_l1id     & 0xFFF)  << (50-31);

}


void Malta2Data::unPack(){
  unpack();
}

void Malta2Data::unpack(){
  m_refbit   = (m_word1 >>  0) & 0x1;
  m_pixel    = (m_word1 >>  1) & 0xFFFF;
  m_group    = (m_word1 >> 17) & 0x1F;
  m_parity   = (m_word1 >> 22) & 0x1;
  m_delay    = (m_word1 >> 23) & 0x7;
  m_dcolumn  = (m_word1 >> 26) & 0x1F;
  m_dcolumn |= ((m_word2 & 0x7) << 5);
  m_chipbcid = (m_word2 >> (34-31) & 0x3);
  m_chipid   = (m_word2 >> (36-31) & 0x1);
  m_phase    = (m_word2 >> (37-31) & 0x7);
  m_winid    = (m_word2 >> (40-31) & 0x7); // was 0xF
  m_bcid     = (m_word2 >> (44-31) & 0x1F); // was 0x3F
  m_l1id     = (m_word2 >> (50-31) & 0xFFF);

  m_nhits = 0;
  for(uint32_t i=0; i<16;i++){
    if(((m_pixel>>i)&0x1)==0){continue;}

    int column = m_dcolumn*2;
    if(i>7){column+=1;}

    int row = m_group*16;
    if(m_parity==1){row+=8;}
    if     (i==0 || i== 8){row+=0;}
    else if(i==1 || i== 9){row+=1;}
    else if(i==2 || i==10){row+=2;}
    else if(i==3 || i==11){row+=3;}
    else if(i==4 || i==12){row+=4;}
    else if(i==5 || i==13){row+=5;}
    else if(i==6 || i==14){row+=6;}
    else if(i==7 || i==15){row+=7;}
    m_rows[m_nhits]=row;
    m_columns[m_nhits]=column;
    m_nhits++;
  }
}

void Malta2Data::setHit(uint32_t col, uint32_t row){

  m_refbit = 1;
  row=row&0x1FF;
  col=col&0x1FF;
  m_dcolumn = col>>1;
  m_group = row >> 4; //divide by 16 (32 groups)
  uint32_t rest = row - (m_group<<4); //multiply by 16
  m_parity = (rest>7?1:0);
  uint32_t pbit = (row%8) + (col%2==1?8:0);
  m_pixel = 1 << pbit;

}

string Malta2Data::toString(){
  ostringstream os;
  os << "|" << bitset<12>(m_l1id)
     << "|" << bitset< 6>(m_bcid)
     << "|" << bitset< 4>(m_winid)
     << "|" << bitset< 3>(m_phase)
     << "|" << bitset< 1>(m_chipid)
     << "|" << bitset< 8>(m_dcolumn)
     << "|" << bitset< 3>(m_delay)
     << "|" << bitset< 1>(m_parity)
     << "|" << bitset< 5>(m_group)
     << "|" << bitset<16>(m_pixel)
     << "|" << bitset< 1>(m_refbit)
     << "|";
  return os.str();

}

string Malta2Data::getInfo(){
  ostringstream os;
  os << "Refbit: " << getRefbit()
     << "  Pixel: " << getPixel()
     << "  Group: " << getGroup()
     << "  Parity: " << getParity()
     << "  Delay: " << getDelay()
     << "  DoubleCol: " << getDcolumn()
     << "  CHIPBCID: " << getChipbcid()
     << "  Phase: " << getPhase()
     << "  WinID: " << getWinid()
     << "  L1ID: " << getL1id()
     << "  BCID: " << getBcid();
  return os.str();
}

void Malta2Data::dump(){
  cout << getInfo() << endl;
}
