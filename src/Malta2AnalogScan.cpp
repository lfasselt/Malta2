//! -*-C++-*-
#include "Malta2/Malta2.h"
#include "Malta2/Malta2Data.h"
#include "Malta2/Malta2Tree.h"
#include "Malta2/Malta2Utils.h"
#include "Malta2/Malta2Masker.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <signal.h>
#include <iomanip>
#include <chrono>
#include <time.h>
#include <algorithm>
#include <stdlib.h>  
#include <sys/stat.h>

#include "TCanvas.h"
#include "TStyle.h"

#include <cmdl/cmdargs.h>

using namespace std;
bool g_cont=true;

void handler(int)
{
  cout << "You pressed ctrl+c to quit" << endl;
  g_cont=false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int main(int argc, char *argv[])
{
  //time
  time_t start,end;

  cout << "#####################################" << endl
       << "# Welcome to MALTA2 Analog Scan     #" << endl
       << "#####################################" << endl;
  
  CmdArgBool    cVerbose('v',"verbose","turn on verbose mode");
  CmdArgStr     cAddress('a',"address","address","connection string udp://ip:port.",CmdArg::isREQ);
  CmdArgStr     cChip('c',"chip","sample","WXRX",CmdArg::isREQ);
  CmdArgStr     cTag('t',"tag","text","test description. Defatult test");
  CmdArgInt     cVPulseLo('l',"vlo","integer","VPULSE_LOW value [5-127]. Default 10");
  CmdArgInt     cVPulseHi('h',"vhi","integer","VPULSE_HIGH value [5-127]. Default 70");
  CmdArgInt     cCol0('x',"col0", "number", "first column [0-511]. Default 0");
  CmdArgInt     cColN('X',"colN", "number", "last column [0-511]. Default 511");
  CmdArgInt     cRow0('y',"row0", "number", "first row [288-511]. Default 288");
  CmdArgInt     cRowN('Y',"rowN", "number", "last row [288-511]. Default 511"); 
  CmdArgInt     cIDB('i',"idb", "number", "IDB value [10-200]. Default 50");
  CmdArgInt     cNumTrigs('n',"numtrigs","trigger","number of triggers. Default 100");
  CmdArgBool    cQuiet('q',"quiet","do not write root files");
  CmdArgStr     cConfig('C',"config","config","configuration file");
  
  cVPulseLo=10;
  cVPulseHi=70;
  cQuiet=false;
  cNumTrigs=50;
  cCol0=0;
  cRow0=288;
  cColN=511;
  cRowN=511;
  cIDB=50;

  CmdLine cmdl(*argv,
	       &cVerbose,
	       &cAddress,
	       &cChip,
	       &cTag,
	       &cVPulseLo,
	       &cVPulseHi,
	       &cIDB,
	       &cCol0,
	       &cColN,
	       &cRow0,
	       &cRowN,
	       &cNumTrigs,
	       &cQuiet,
	       &cConfig,
  	       NULL);

  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter); 
  
  //making output directory
  string outdir=getenv("MALTA_DATA_PATH");
  mkdir(outdir.c_str(), S_IFDIR| S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  outdir+="/Malta2";
  mkdir(outdir.c_str(), S_IFDIR| S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  outdir+="/Results_AnalogScan";
  mkdir(outdir.c_str(), S_IFDIR| S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  outdir+="/"+string(cChip);
  mkdir(outdir.c_str(), S_IFDIR| S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  outdir+="/"+(cTag.flags() & CmdArg::GIVEN?string(cTag):"test");
  mkdir(outdir.c_str(), S_IFDIR| S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

  //Connect to MALTA2
  Malta2 * malta = new Malta2();
  malta->Connect(string(cAddress));
  
  malta->SetVerbose(false);
  malta->WriteConstDelays(4,1);
  malta->SetExternalL1A(false);
  malta->SetReadoutDelay(80);

  string nTrigger = to_string(cNumTrigs);
  string charge  = "high";
  string title = "Number of hits, N="+nTrigger+", Q="+charge+" e;Pix X; Pix Y";
  TH2I* nHits_h2= new TH2I("Hits",title.c_str(), 512,-0.5,511.5,512,-0.5,511.5);
  TH2I* pPulsed_h2= new TH2I("pulsedPixels","Pulsed Pixels", 512,-0.5,511.5,512,-0.5,511.5);

  cout << "#####################################" << endl
       << "# Configuring chip                  #" << endl
       << "#####################################" << endl;

  Malta2Utils::PreConfigureMalta(malta);
  
  if (cConfig.flags() & CmdArg::GIVEN){
    if(!malta->SetConfigFromFile(string(cConfig))){
      cout << "## Cannot parse config file: " << string(cConfig) << endl;
      cout << "## Exit"<<endl;
      delete malta;
      exit(0);
    }
  }else{
    cout << "## Using default config from Malta2Utils" << endl;
  }

  malta->SetVPULSE_HIGH(cVPulseHi);
  malta->SetVPULSE_LOW(cVPulseLo);
  malta->SetIDB(cIDB);
  Malta2Utils::EnableAll(malta);

  //Build the masker
  Malta2Masker masker;
  masker.SetVerbose(cVerbose);
  masker.SetRange(cCol0,cRow0,cColN,cRowN);
  masker.SetShape(2,8);
  masker.SetFrequency(1);
  masker.Build();
  
  //Create ntuple
  ostringstream os;
  os.str("");
  os << outdir  << "/analog_scan_data.root";
  string fname = os.str();
  Malta2Tree *ntuple = new Malta2Tree();
  if (!cQuiet){
    ntuple->Open(fname,"RECREATE");
    ntuple->Extend();
    ntuple->SetIDB(cIDB);
    ntuple->SetVHIGH(cVPulseHi);
    ntuple->SetVLOW(cVPulseLo);
  }
  
  cout << "#####################################" << endl
       << "# Start Mask loop                   #" << endl
       << "#####################################" << endl;
  
  time(&start);//all pixel scanning clock start
  signal(SIGINT,handler);
  int pixels=0;

  for(uint32_t step=0; step<masker.GetNumSteps(); step++){
    if(!g_cont) break;
    masker.GetStep(step);
    if(!cQuiet){ ntuple->SetL1idC(step); }
      
    //Unmask desired pixel
    cout << "Step " << (step+1) << "/" << masker.GetNumSteps() << endl;
      
    for (uint32_t r=0; r<512; r++) malta->SetPixelPulseRow(r,false);
    for (uint32_t c=0; c<512; c++) malta->SetPixelPulseColumn(c,false);
    for (uint32_t a=0; a<256; a++) malta->SetDoubleColumnMask(a,true);

    for(pair<uint32_t,uint32_t> pix : masker.GetPixels()){
      pixels+=1;
      pPulsed_h2->Fill(pix.first, pix.second);           
      malta->SetPixelPulseColumn(pix.first,true);
      malta->SetPixelPulseRow(pix.second,true);
    }

    for(uint32_t dc : masker.GetDoubleColumns()){
      malta->SetDoubleColumnMask(dc,false);
    }

    //actually configure malta2
    Malta2Utils::MaltaSend(malta);
      
    if(!g_cont) break;
      
    
    //Flush MALTA
    malta->ResetFifo();
    malta->ReadoutOn();
    malta->Trigger(cNumTrigs,true);
    malta->ReadoutOff();
    
    Malta2Data md,md2;
    int maxwords=200;
    uint32_t words[200];
    for (uint32_t i=0;i<maxwords;i++){ words[i]=0;}
    
    bool isFirst=true;
    int numberOfHits=0;     

    //Readout 
    while(g_cont){	      
      int numwords = malta->GetFIFO2WordCount();
      if(numwords > maxwords){ numwords = maxwords; }
      
      malta->ReadMaltaWord(words,numwords);
      
      // This is the way out of the loop.
      if (words[0]==0){
	break;
      }
      
      for (uint32_t i = 0; i < numwords; i+=2){		      
	if (isFirst){
	  md.setWord1(words[0]);
	  md.setWord2(words[1]);
	  isFirst = false;
	  continue;
	}
	
	md2.setWord1(md.getWord1());
	md2.setWord2(md.getWord2());
	md.setWord1(words[0+i]);
	md.setWord2(words[1+i]);
	md.unPack();
	md2.unPack();
	
	bool markDuplicate = Malta2Utils::markDuplicate(md2.getBcid(),md.getBcid(),
							md2.getWinid(),md.getWinid(),
							md2.getL1id(),md.getL1id(),
							md2.getPhase(),md.getPhase());
	
	if (!cQuiet){
	  ntuple->Set(&md2);
	  ntuple->SetIsDuplicate(markDuplicate?1:0);
	  ntuple->Fill();
	}
	if (markDuplicate==0 ){ 
	  for (unsigned h=0; h<md2.getNhits(); h++){
	    uint32_t pixX=md2.getHitColumn(h);
	    uint32_t pixY=md2.getHitRow(h);
	    if(cVerbose) cout << "hit: " << pixX << "," << pixY << endl;
	    if (masker.Contains(pixX,pixY)){ 
	      numberOfHits++;
	      nHits_h2->Fill(pixX, pixY);		 
	    }
	  }
	}
      }
    }

    cout << "Number of hits in selected Pixels: " << numberOfHits << endl;
    
  }

  ///////////////////////////////////////////////////////
  time(&end);
  
  cout << endl << "Total scantime for " << pixels << " pixels: " << (end-start) << " seconds" << endl;

  if (!cQuiet) ntuple->Close();
  delete ntuple;
  delete malta;
  
  //plot for analog scan
  TCanvas* can = new TCanvas("canN","caN",1600,800);
  gStyle->SetOptStat(0);
  gStyle->SetPalette(1);
  nHits_h2->GetXaxis()->SetRangeUser(cCol0,cColN);
  nHits_h2->GetYaxis()->SetRangeUser(cRow0,cRowN);
  nHits_h2->Draw("colz");
  can->Print( (outdir+"/nHits.pdf").c_str() );
    
  TFile *outputF = new TFile((outdir+"/analog_scan_results.root").c_str(), "RECREATE");
  can->Write();
  nHits_h2->Write();
  pPulsed_h2->Write();
  outputF->Close();
  
  int n0{0}, nFewer{0}, nMore{0}, nClose{0}, nCloser{0};
  
  for (int i =1; i<513; i++)
    {
      for (int j =1; j<513; j++)
	{
	  if (pPulsed_h2->GetBinContent(i,j)==0) continue;
	  if (nHits_h2->GetBinContent(i,j)==0) n0++;
	  if (nHits_h2->GetBinContent(i,j)<cNumTrigs * 0.9 ) nFewer++;
	  if (nHits_h2->GetBinContent(i,j)>cNumTrigs *1.1)  nMore++;
	  if ((abs(nHits_h2->GetBinContent(i,j)- cNumTrigs) <= 0.1*cNumTrigs) 
	      and abs(nHits_h2->GetBinContent(i,j)- cNumTrigs) > 0.05*cNumTrigs  ) 
	    nClose++;
	  if (abs(nHits_h2->GetBinContent(i,j)- cNumTrigs) <= 0.05*cNumTrigs ) nCloser++;
	}
    } 
  cout << "Thanks for waiting ... \n\n RESULTS" << std::endl;
  cout << "# of pixels with 0 hits: "<<n0 <<endl;
  cout << "# of pixels with # of hits < 90%  nTrigger: "<<nFewer <<endl;
  cout << "# of pixels with # of hits > 110% nTrigger: "<<nMore <<endl;
  cout << "# of pixels # of hits within 10% of nTriggger: "<<nClose <<endl;  
  cout << "# of pixels # of hits within 5% of nTriggger: "<<nCloser <<endl;  
  
  delete outputF;
  delete pPulsed_h2;
  delete can;
  delete nHits_h2;

  cout << "Have a nice day!" << endl;

  return 0; // all done
}
