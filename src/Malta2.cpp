//Malta2 class
#include "Malta2/Malta2.h"
#include <math.h>
#include <map>
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <bitset>
#include <string>
#include <fstream>
#include <TFile.h>
#include <TH2I.h>
#include <TH1D.h>

using namespace std;

Malta2::Malta2(){
  verbose=false;
  m_capacitance=300;
  initialize_array();
  
  //Slow control definitions
  defs[DATAFLOW_MERGERTOLEFT]   = 0x1;
  defs[DATAFLOW_MERGERTORIGHT]  = 0x0;    // def = 0
  defs[DATAFLOW_LMERGERTOLVDS]  = 0x1;    // def = 0
  defs[DATAFLOW_LMERGERTOCMOS]  = 0x1;    // def = 0
  defs[DATAFLOW_RMERGERTOLVDS]  = 0x0;
  defs[DATAFLOW_RMERGERTOCMOS]  = 0x0;
  defs[DATAFLOW_LCMOS]          = 0x0;
  defs[DATAFLOW_RCMOS]          = 0x0;
  defs[DATAFLOW_ENLVDS]         = 0x1;
  defs[DATAFLOW_ENLCMOS]        = 0x1;
  defs[DATAFLOW_ENRCMOS]        = 0x0;
  defs[DATAFLOW_ENMERGER]       = 0x0;
  defs[POWER_SWITCH_LEFT]       = 0x1;
  defs[POWER_SWITCH_RIGHT]      = 0x0;
  defs[LVDS_EN]                 = 0x1;
  defs[LVDS_PRE]                = 0xFF00; //0xFF;
  defs[LVDS_BRIDGE_EN]          = 0x7;    //0x1C;
  defs[LVDS_CMFB_EN]            = 0x7;    //0x1C;
  defs[LVDS_SET_IBCMFB]         = 0xE;    //0x7;
  defs[LVDS_SET_IVPH]           = 0x4;    //0x2;
  defs[LVDS_SET_IVPL]           = 0xB;    //0xD;
  defs[LVDS_SET_IVNH]           = 0xB;    //0xD;
  defs[LVDS_SET_IVNL]           = 0xE;    //0x7;
  defs[SWCNTL_VCASN]            = 0x0;
  defs[SWCNTL_VCLIP]            = 0x0;
  defs[SWCNTL_VPULSE_HIGH]      = 0x0;
  defs[SWCNTL_VPULSE_LOW]       = 0x0;
  defs[SWCNTL_VRESET_P]         = 0x0;
  defs[SWCNTL_VRESET_D]         = 0x0;
  defs[SWCNTL_ICASN]            = 0x0;
  defs[SWCNTL_IRESET]           = 0x0;
  defs[SWCNTL_IBIAS]            = 0x0;
  defs[SWCNTL_ITHR]             = 0x0;
  defs[SWCNTL_IDB]              = 0x0;
  defs[SWCNTL_IREF]             = 0x0;
  defs[SET_IRESET_BIT]          = 0x1;
  defs[SWCNTL_DACMONV]          = 0x0;
  defs[SWCNTL_DACMONI]          = 0x0;
  defs[SET_IBUFP_MON_0]         = 0xA;    //0x5;
  defs[SET_IBUFN_MON_0]         = 0x9;
  defs[SET_IBUFP_MON_1]         = 0xA;    //0x5;
  defs[SET_IBUFN_MON_1]         = 0x9;
  defs[PULSE_MON_L]             = 0x1;    //Changed by Leyre to have the last bit to 1 def 0
  defs[PULSE_MON_R]             = 0x1;    //Changed by Leyre to have the last bit to 1 def 0
  defs[LVDS_VBCMFB]             = 0xD;    //0xE
  //FIFOs
  defs[SET_VCASN]               = 110;
  defs[SET_VCLIP]               = 125;
  defs[SET_VPULSE_HIGH]         =  90;
  defs[SET_VPULSE_LOW]          =  10;
  defs[SET_VRESET_P]            =  29;
  defs[SET_VRESET_D]            =  65;
  defs[SET_ICASN]               =   5;
  defs[SET_IRESET]              =  30;
  defs[SET_ITHR]                = 120;
  defs[SET_IBIAS]               =  43;
  defs[SET_IDB]                 = 120;
 
 
    
  names[DATAFLOW_MERGERTOLEFT]       = "DATAFLOW_MERGERTOLEFT";
  names[DATAFLOW_MERGERTORIGHT]      = "DATAFLOW_MERGERTORIGHT";
  names[DATAFLOW_LMERGERTOLVDS]      = "DATAFLOW_LMERGERTOLVDS";
  names[DATAFLOW_LMERGERTOCMOS]      = "DATAFLOW_LMERGERTOCMOS";
  names[DATAFLOW_RMERGERTOLVDS]      = "DATAFLOW_RMERGERTOLVDS";
  names[DATAFLOW_RMERGERTOCMOS]      = "DATAFLOW_RMERGERTOCMOS";
  names[DATAFLOW_LCMOS]              = "DATAFLOW_LCMOS";
  names[DATAFLOW_RCMOS]              = "DATAFLOW_RCMOS";
  names[DATAFLOW_ENLVDS]             = "DATAFLOW_ENLVDS";
  names[DATAFLOW_ENLCMOS]            = "DATAFLOW_ENLCMOS";
  names[DATAFLOW_ENRCMOS]            = "DATAFLOW_ENRCMOS";
  names[DATAFLOW_ENMERGER]           = "DATAFLOW_ENMERGER";
  names[POWER_SWITCH_LEFT]           = "POWER_SWITCH_LEFT";
  names[POWER_SWITCH_RIGHT]          = "POWER_SWITCH_RIGHT";
  names[LVDS_EN]                     = "LVDS_EN";
  names[LVDS_PRE]                    = "LVDS_PRE";
  names[LVDS_BRIDGE_EN]              = "LVDS_BRIDGE_EN";
  names[LVDS_CMFB_EN]                = "LVDS_CMFB_EN";
  names[LVDS_SET_IBCMFB]             = "LVDS_SET_IBCMFB";
  names[LVDS_SET_IVPH]               = "LVDS_SET_IVPH";
  names[LVDS_SET_IVPL]               = "LVDS_SET_IVPL";
  names[LVDS_SET_IVNH]               = "LVDS_SET_IVNH";
  names[LVDS_SET_IVNL]               = "LVDS_SET_IVNL";
  names[SWCNTL_VCASN]                = "SWCNTL_VCASN";
  names[SWCNTL_VCLIP]                = "SWCNTL_VCLIP";
  names[SWCNTL_VPULSE_HIGH]          = "SWCNTL_VPULSE_HIGH";
  names[SWCNTL_VPULSE_LOW]           = "SWCNTL_VPULSE_LOW";
  names[SWCNTL_VRESET_P]             = "SWCNTL_VRESET_P";
  names[SWCNTL_VRESET_D]             = "SWCNTL_VRESET_D";
  names[SWCNTL_ICASN]                = "SWCNTL_ICASN";
  names[SWCNTL_IRESET]               = "SWCNTL_IRESET";
  names[SWCNTL_IBIAS]                = "SWCNTL_IBIAS";
  names[SWCNTL_ITHR]                 = "SWCNTL_ITHR";
  names[SWCNTL_IDB]                  = "SWCNTL_IDB";
  names[SWCNTL_IREF]                 = "SWCNTL_IREF";
  names[SET_IRESET_BIT]              = "SET_IRESET_BIT";
  names[SWCNTL_DACMONV]              = "SWCNTL_DACMONV";
  names[SWCNTL_DACMONI]              = "SWCNTL_DACMONI";
  names[SET_IBUFP_MON_0]             = "SET_IBUFP_MON_0";
  names[SET_IBUFN_MON_0]             = "SET_IBUFN_MON_0";
  names[SET_IBUFP_MON_1]             = "SET_IBUFP_MON_1";
  names[SET_IBUFN_MON_1]             = "SET_IBUFN_MON_1";

  names[SET_VCASN]                   = "SC_VCASN";
  names[SET_VCLIP]                   = "SC_VCLIP";
  names[SET_VPULSE_HIGH]             = "SC_VPULSE_HIGH";
  names[SET_VPULSE_LOW]              = "SC_VPULSE_LOW";
  names[SET_VRESET_P]                = "SC_VRESET_P";
  names[SET_VRESET_D]                = "SC_VRESET_D";
  names[SET_ICASN]                   = "SC_ICASN";
  names[SET_IRESET]                  = "SC_IRESET";
  names[SET_ITHR]                    = "SC_ITHR";
  names[SET_IBIAS]                   = "SC_IBIAS";
  names[SET_IDB]                     = "SC_IDB";

  names[MASK_COL]                    = "MASK_COL";
  names[MASK_HOR]                    = "MASK_HOR";
  names[MASK_DIAG]                   = "MASK_DIAG";
  names[MASK_FULLCOL]                = "MASK_DOUBLE_COLUMN";
  names[PULSE_COL]                   = "PULSE_COL";
  names[PULSE_HOR]                   = "PULSE_HOR";
  names[PULSE_MON_L]                 = "PULSE_MON_L";
  names[PULSE_MON_R]                 = "PULSE_MON_R";
  names[LVDS_VBCMFB]                 = "LVDS_VBCMFB";
  names[MASK_DOUBLE_COLUMNS]         = "MASK_DOUBLE_COLUMNS";
  names[MASK_PIXEL]                  = "MASK_PIXEL";
  names[ROI]                         = "ROI";
  names[MASK_PIXELS_FROM_TXT]        = "MASK_PIXELS_FROM_TXT";
  names[TAP_FROM_TXT]                = "TAP_FROM_TXT";
  
  
  addresses["DATAFLOW_MERGERTOLEFT"]       = DATAFLOW_MERGERTOLEFT;
  addresses["DATAFLOW_MERGERTORIGHT"]      = DATAFLOW_MERGERTORIGHT;
  addresses["DATAFLOW_LMERGERTOLVDS"]      = DATAFLOW_LMERGERTOLVDS;
  addresses["DATAFLOW_LMERGERTOCMOS"]      = DATAFLOW_LMERGERTOCMOS;
  addresses["DATAFLOW_RMERGERTOLVDS"]      = DATAFLOW_RMERGERTOLVDS;
  addresses["DATAFLOW_RMERGERTOCMOS"]      = DATAFLOW_RMERGERTOCMOS;
  addresses["DATAFLOW_LCMOS"]              = DATAFLOW_LCMOS;
  addresses["DATAFLOW_RCMOS"]              = DATAFLOW_RCMOS;
  addresses["DATAFLOW_ENLVDS"]             = DATAFLOW_ENLVDS;
  addresses["DATAFLOW_ENLCMOS"]            = DATAFLOW_ENLCMOS;
  addresses["DATAFLOW_ENRCMOS"]            = DATAFLOW_ENRCMOS;
  addresses["DATAFLOW_ENMERGER"]           = DATAFLOW_ENMERGER;
  addresses["POWER_SWITCH_LEFT"]           = POWER_SWITCH_LEFT;
  addresses["POWER_SWITCH_RIGHT"]          = POWER_SWITCH_RIGHT;
  addresses["LVDS_EN"]                     = LVDS_EN;
  addresses["LVDS_PRE"]                    = LVDS_PRE;
  addresses["LVDS_BRIDGE_EN"]              = LVDS_BRIDGE_EN;
  addresses["LVDS_CMFB_EN"]                = LVDS_CMFB_EN;
  addresses["LVDS_SET_IBCMFB"]             = LVDS_SET_IBCMFB;
  addresses["LVDS_SET_IVPH"]               = LVDS_SET_IVPH;
  addresses["LVDS_SET_IVPL"]               = LVDS_SET_IVPL;
  addresses["LVDS_SET_IVNH"]               = LVDS_SET_IVNH;
  addresses["LVDS_SET_IVNL"]               = LVDS_SET_IVNL;
  addresses["SWCNTL_VCASN"]                = SWCNTL_VCASN;
  addresses["SWCNTL_VCLIP"]                = SWCNTL_VCLIP;
  addresses["SWCNTL_VPULSE_HIGH"]          = SWCNTL_VPULSE_HIGH;
  addresses["SWCNTL_VPULSE_LOW"]           = SWCNTL_VPULSE_LOW;
  addresses["SWCNTL_VRESET_P"]             = SWCNTL_VRESET_P;
  addresses["SWCNTL_VRESET_D"]             = SWCNTL_VRESET_D;
  addresses["SWCNTL_ICASN"]                = SWCNTL_ICASN;
  addresses["SWCNTL_IRESET"]               = SWCNTL_IRESET;
  addresses["SWCNTL_IBIAS"]                = SWCNTL_IBIAS;
  addresses["SWCNTL_ITHR"]                 = SWCNTL_ITHR;
  addresses["SWCNTL_IDB"]                  = SWCNTL_IDB;
  addresses["SWCNTL_IREF"]                 = SWCNTL_IREF;
  addresses["SET_IRESET_BIT"]              = SET_IRESET_BIT;
  addresses["SWCNTL_DACMONV"]              = SWCNTL_DACMONV;
  addresses["SWCNTL_DACMONI"]              = SWCNTL_DACMONI;
  addresses["SET_IBUFP_MON_0"]             = SET_IBUFP_MON_0;
  addresses["SET_IBUFN_MON_0"]             = SET_IBUFN_MON_0;
  addresses["SET_IBUFP_MON_1"]             = SET_IBUFP_MON_1;
  addresses["SET_IBUFN_MON_1"]             = SET_IBUFN_MON_1;
  addresses["SC_VCASN"]                   = SET_VCASN;
  addresses["SC_VCLIP"]                   = SET_VCLIP;
  addresses["SC_VPULSE_HIGH"]             = SET_VPULSE_HIGH;
  addresses["SC_VPULSE_LOW"]              = SET_VPULSE_LOW;
  addresses["SC_VRESET_P"]                = SET_VRESET_P;
  addresses["SC_VRESET_D"]                = SET_VRESET_D;
  addresses["SC_ICASN"]                   = SET_ICASN;
  addresses["SC_IRESET"]                  = SET_IRESET;
  addresses["SC_ITHR"]                    = SET_ITHR;
  addresses["SC_IBIAS"]                   = SET_IBIAS;
  addresses["SC_IDB"]                     = SET_IDB;
  addresses["MASK_COL"]                    = MASK_COL;
  addresses["MASK_HOR"]                    = MASK_HOR;
  addresses["MASK_DIAG"]                   = MASK_DIAG;
  addresses["MASK_DOUBLE_COLUMN"]          = MASK_FULLCOL;
  addresses["PULSE_COL"]                   = PULSE_COL;
  addresses["PULSE_HOR"]                   = PULSE_HOR;
  addresses["PULSE_MON_L"]                 = PULSE_MON_L;
  addresses["PULSE_MON_R"]                 = PULSE_MON_R;
  addresses["LVDS_VBCMFB"]                 = LVDS_VBCMFB;
  addresses["MASK_DOUBLE_COLUMNS"]         = MASK_DOUBLE_COLUMNS;
  addresses["MASK_PIXEL"]                  = MASK_PIXEL;
  addresses["ROI"]                         = ROI;
  addresses["MASK_PIXELS_FROM_TXT"]        = MASK_PIXELS_FROM_TXT;
  addresses["TAP_FROM_TXT"]                = TAP_FROM_TXT;
  
  m_mask_row.resize(512,0);
  m_mask_col.resize(512,0);
  m_mask_diag.resize(512,0);
  m_mask_dc.resize(256,0);
 
}

Malta2::~Malta2(){}

void Malta2::SetVerbose(bool enable){
  verbose=enable;
  cout << "Malta2::SetVerbose("<< enable <<")"<< endl;
  //m_ipb->SetVerbose(enable);
}

void Malta2::SetIPbus(ipbus::Uhal * ipb){
  m_ipb=ipb;
  CheckConnection();
}

ipbus::Uhal * Malta2::GetIPbus(){
  return m_ipb;
}

bool Malta2::Connect(std::string connstr){
  m_ipb = new ipbus::Uhal(connstr);
  return CheckConnection();
}

bool Malta2::CheckConnection(){
  uint32_t version=0;
  m_ipb->Read(IPBADDR_VERSION, version);  
  if (version==0){
    cout << endl 
	 << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl 
	 << "MALTA2 WRONG FW VERSION OR COMMUNICATION ERROR" << endl
	 << " Version: " << version << endl
	 << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
    return false;
  }else{
    if(verbose) {cout << "Malta2::CheckConnection version: " << hex << version << endl;}
  }
  return true;
}

bool Malta2::IsPixelMasked(uint32_t col, uint32_t row){
  for (auto pix: m_maskedPixelList){
    if(pix.first==col && pix.second==row){return true;}
  }
  return false;
}

TH2I* Malta2::GetPixelMask(){
  TH2I * m_pixelMask = new TH2I("pixelmask","pixelmask",512,-0.5,511.5,512,-0.5,511.5);
  for(auto pix: m_maskedPixelList){
    if(m_pixelMask->GetBinContent(pix.first+1,pix.second+1) == 0){
      m_pixelMask->Fill(pix.first,pix.second);
    }
  }
  return m_pixelMask;  
}

TH1D* Malta2::GetDColMask(){
  TH1D * m_dcMask = new TH1D("dcolmask","dcolmask",256,0,256);

  for(uint32_t dc=0;dc<256;dc++){
    if(!m_mask_dc[dc]){m_dcMask->Fill(dc);}
  }
  
  return m_dcMask;
}

TH2I* Malta2::GetMaskedPixels(int roiXmin, int roiYmin, int roiXmax, int roiYmax){

  TH2I * m_maskedPixels = new TH2I("maskedPixels","maskedPixels",512,-0.5,511.5,512,-0.5,511.5);

  std::vector<int> maskedCols, maskedRows, maskedDiags;
  
  if(!(roiXmin == 0 && roiXmax == 0 && roiYmin == 0 && roiYmax == 0)){
    //mask double columns outright
    for(int i = 0; i < roiXmin; i+=2){
      for(int j = 0; j < 511; ++j){
        m_maskedPixels->SetBinContent(i+1,j+1,3);
        m_maskedPixels->SetBinContent(i+2,j+1,3);
      }
    }
    for(int i = roiXmax; i < 511; i+=2){
      for(int j = 0; j < 511; ++j){
        m_maskedPixels->SetBinContent(i+1,j+1,3);
        m_maskedPixels->SetBinContent(i+2,j+1,3);
      }
    }
    
    for(int i = 0; i < 511; i+=2){
      for(int j = 0; j < roiYmin; ++j){
        m_maskedPixels->SetBinContent(i+1,j+1,3);
        m_maskedPixels->SetBinContent(i+2,j+1,3);
      }
      for(int j = roiYmax; j < 511; ++j){
        m_maskedPixels->SetBinContent(i+1,j+1,3);
        m_maskedPixels->SetBinContent(i+2,j+1,3);
      }
    }
   }

  for(auto pix: m_maskedPixelList){    
    int diag = (pix.second - pix.first)%512;
    //int diag = m_msc->getDiagonal( pix.first, pix.second);
    if  (std::find(maskedCols.begin(),maskedCols.end(),pix.first) == maskedCols.end()){
      for(int i = 0; i < 512; ++i) m_maskedPixels->Fill(pix.first, i);
      maskedCols.push_back(pix.first);
    }
    if (std::find(maskedRows.begin(),maskedRows.end(),pix.second) == maskedRows.end()){
      for(int i = 0; i < 512; ++i) m_maskedPixels->Fill(i, pix.second);
      maskedRows.push_back(pix.second);
    }
    if (std::find(maskedDiags.begin(),maskedDiags.end(),diag) == maskedDiags.end() ){
      for(int j = 0; j < 512; ++j)    m_maskedPixels->Fill(j,(j+diag)%512);
      maskedDiags.push_back(diag);
    } 
  }

  Int_t nMaskedPixels;
  Double_t nMaskedPixelsFrac = 0.0;
  if(!(roiXmin == 0 && roiXmax == 0 && roiYmin == 0 && roiYmax == 0)){
    nMaskedPixels = (Int_t)m_maskedPixels->Integral(roiXmin,roiXmax,roiYmin,roiYmax);
    nMaskedPixelsFrac = (Double_t)nMaskedPixels/((roiXmax-roiXmin)*(roiYmax-roiYmin))*100.0;
  } else {
    nMaskedPixels = (Int_t)m_maskedPixels->Integral();
    nMaskedPixelsFrac = (Double_t)nMaskedPixels/(512.*224.)*100.0;
  }

  for(int i = 0; i < 513; ++i){
    for(int j = 0; j < 513; ++j){
      if (m_maskedPixels->GetBinContent(i,j) >= 3) {m_maskedPixels->SetBinContent(i,j,1); 
      }else m_maskedPixels->SetBinContent(i,j,0);
    }
  }

  cout<< "The total number of masked pixels is : "<< m_maskedPixels->Integral()<<endl;
  m_maskedPixels->SetMaximum(3);
  m_maskedPixels->SetTitle(("Masked Pixels (N = "+std::to_string(m_maskedPixels->Integral())+" = "+std::to_string(nMaskedPixelsFrac).substr(0,5)+"%); pixX; pixY").c_str());
  return m_maskedPixels;
}

void Malta2::Reset(){
  uint32_t mask= 0xFFFFFFFF ^ (1<<12);
  uint32_t cacheWord;
  //cout << "RESET!!!" << endl;
  m_ipb->Read(8,cacheWord);
  m_ipb->Write(8, cacheWord | (1<<12) );
  usleep(500000); //MLB
  m_ipb->Write(8, cacheWord & mask );
}     

void Malta2::SetDefaults(bool force){
  // FLIP MASK_COL, MASK_HOR and MASK_FULLCOL  
  for (uint32_t a=0;a<512;a++){
    defsFIFO[MASK_COL][a]=1;
    defsFIFO[MASK_HOR][a]=1;
  }
  for (uint32_t a=0;a<256;a++){
    defsFIFO[MASK_FULLCOL][a]=1;
  }
 
  // LOAD FROM DEFS
  if(verbose) cout << "SetDefaut(" << std::boolalpha << force << ")" <<  endl;
  for( uint32_t a = SIMPLE_WORDS_0; a <= SIMPLE_WORDS_N; a++) {
    m_values[a]=defs[a];
  }
  int c=0;
  for(uint32_t  mx = 0; mx <=16; mx++) {
    uint32_t size_of_fifo=m_fifoSizes[mx];
    if(verbose) cout << "Setting default....\t[" <<names[mx]<<"]\t\t["<< size_of_fifo <<"]bits" <<  endl;
    for(uint32_t i = 0; i < size_of_fifo; i++) {
      //cout << " m_fifos[" << mx <<  "]["  <<i<< "]=" << defsFIFO[mx][i] << endl; 
      m_fifos[mx][i]=defsFIFO[mx][i];
      c=c + 1;
     }
  }
  //Update defs integer number of some fifos
  Write(SET_VCASN      , defs[SET_VCASN]      , false);
  Write(SET_VCLIP      , defs[SET_VCLIP]      , false);
  Write(SET_VPULSE_HIGH, defs[SET_VPULSE_HIGH], false);
  Write(SET_VPULSE_LOW , defs[SET_VPULSE_LOW] , false);
  Write(SET_VRESET_P   , defs[SET_VRESET_P]   , false);
  Write(SET_VRESET_D   , defs[SET_VRESET_D]   , false);
  Write(SET_ICASN      , defs[SET_ICASN]      , false);
  Write(SET_IRESET     , defs[SET_IRESET]     , false);
  Write(SET_ITHR       , defs[SET_ITHR]       , false);
  Write(SET_IBIAS      , defs[SET_IBIAS]      , false);
  Write(SET_IDB        , defs[SET_IDB]        , false);
  
  if(force){
    WriteAll();
  }
  if(verbose) cout << "End SetDefaults" << endl;
}

uint32_t Malta2::GetDefault(int pos){
  return defs[pos];
}


void Malta2::Write(uint32_t pos, uint32_t value, bool force){
  if (pos < FIFO_WORDS_0 or pos > SIMPLE_WORDS_N){
    // POS DOESN'T EXIST
    //cout << "Error Malta2::Write() position [" << pos << "] is out of range!!" << endl;
  }else if (pos >= SIMPLE_WORDS_0 && pos <= SIMPLE_WORDS_N){ 
    // SIMPLE WORD
    if(verbose) cout << " Write() on [" << names[pos] << "]\t <=\t 0x" << hex << value << dec << endl;
    m_values[pos]=value; 
  }else{
    // FIFO WORD
    //if(verbose) cout << "WriteFIFOword ( pos [ " << pos << " (" << names[pos] << ")]. Value ["<< value << "]" <<  endl;
    if(pos==SET_ICASN or pos==SET_IRESET or pos==SET_ITHR or pos==SET_IBIAS or pos==SET_IDB) {
      //  thermomiter encoder
      //if(verbose) cout << "Thermomiter encoder for " << names[pos] << endl;
      thermomiter_encoder(pos, value);
    }else if (pos==SET_VCASN or pos==SET_VCLIP or pos==SET_VPULSE_HIGH or pos==SET_VPULSE_LOW or pos==SET_VRESET_P or pos==SET_VRESET_D ){
      // one hot encoder
      //if(verbose) cout << "Hot encoder for " << names[pos] << endl;
      hot_encoder(pos, value);
    }else if (pos == PULSE_HOR or pos == PULSE_COL){
      //int flipped=511-value;
      cout << "DONT USE ME" << endl;
      //m_fifos[pos][flipped]=1;
    }else{
      // no enceder
      uint32_t size_of_fifo=m_fifoSizes[pos];//(fifos[pos+1]-fifos[pos]);;
      if(verbose) cout << "No encoding FIFO word  [" << names[pos] << "] of size: " << size_of_fifo <<  endl;
      m_fifos[pos][value] = 1;
    }
  }
}

void Malta2::WriteAll(){
  //Create a vector with all the configuration                                                                       
  // 31 bit 
  if(verbose) cout << "WriteAll()" << endl;
  uint32_t word0=(((m_values[DATAFLOW_MERGERTOLEFT]  & 1)      << 0)|
		  ((m_values[DATAFLOW_MERGERTORIGHT] & 1)      << 1)|
		  ((m_values[DATAFLOW_LMERGERTOLVDS] & 1)      << 2)|
		  ((m_values[DATAFLOW_LMERGERTOCMOS] & 1)      << 3)|
		  ((m_values[DATAFLOW_RMERGERTOLVDS] & 1)      << 4)|
		  ((m_values[DATAFLOW_RMERGERTOCMOS] & 1)      << 5)|
		  ((m_values[DATAFLOW_LCMOS]         & 1 )     << 6)|
		  ((m_values[DATAFLOW_RCMOS]         & 1 )     << 7)|
		  ((m_values[DATAFLOW_ENLVDS]        & 1 )     << 8)|
		  ((m_values[DATAFLOW_ENLCMOS]       & 1 )     << 9)|
		  ((m_values[DATAFLOW_ENRCMOS]       & 1 )     << 10)|
		  ((m_values[DATAFLOW_ENMERGER]      & 1 )     << 11)|
		  ((m_values[POWER_SWITCH_LEFT]      & 1 )     << 12)|
		  ((m_values[POWER_SWITCH_RIGHT]     & 1 )     << 13)|
		  ((m_values[LVDS_EN]                & 1 )     << 14)|
		  ((m_values[LVDS_PRE]               & 0xFFFF) << 15));

  //30 bits
  uint32_t word1=(((m_values[LVDS_BRIDGE_EN]         & 0x1F)   << 0)|
		  ((m_values[LVDS_CMFB_EN]           & 0x1F )  << 5)|
		  ((m_values[LVDS_SET_IBCMFB]        & 0xF)    << 10)|
		  ((m_values[LVDS_SET_IVPH]          & 0xF)    << 14)|
		  ((m_values[LVDS_SET_IVPL]          & 0xF)    << 18)|
		  ((m_values[LVDS_SET_IVNH]          & 0xF)    << 22)|
		  ((m_values[LVDS_SET_IVNL]          & 0xF )   << 26));
  
  // 31 bit 
  
  uint32_t word2=(((m_values[SWCNTL_VCASN]           & 1)      << 0)|
		  ((m_values[SWCNTL_VCLIP]           & 1)      << 1)|
		  ((m_values[SWCNTL_VPULSE_HIGH]     & 1)      << 2)|
		  ((m_values[SWCNTL_VPULSE_LOW]      & 1)      << 3)|
		  ((m_values[SWCNTL_VRESET_P]        & 1)      << 4)|
		  ((m_values[SWCNTL_VRESET_D]        & 1)      << 5)|
		  ((m_values[SWCNTL_ICASN]           & 1 )     << 6)|
		  ((m_values[SWCNTL_IRESET]          & 1 )     << 7)|
		  ((m_values[SWCNTL_IBIAS]           & 1 )     << 8)|
		  ((m_values[SWCNTL_ITHR]            & 1 )     << 9)|
		  ((m_values[SWCNTL_IDB]             & 1 )     << 10)|
		  ((m_values[SWCNTL_IREF]            & 1 )     << 11)|
		  ((m_values[SET_IRESET_BIT]         & 1 )     << 12)|
		  ((m_values[SWCNTL_DACMONV]         & 1 )     << 13)|
		  ((m_values[SWCNTL_DACMONI]         & 1 )     << 14)|
		  ((m_values[SET_IBUFP_MON_0]        & 0xF)    << 15)|
		  ((m_values[SET_IBUFN_MON_0]        & 0xF)    << 19)|
		  ((m_values[SET_IBUFP_MON_1]        & 0xF)    << 23)|
		  ((m_values[SET_IBUFN_MON_1]        & 0xF)    << 27)
		  );
  
  uint32_t word3=(((m_values[PULSE_MON_L]            & 0x1)    << 0)|
		  ((m_values[PULSE_MON_R]            & 0x1 )   << 1)|
		  ((m_values[LVDS_VBCMFB]            & 0xF)    << 2)
		  );
  
  m_values_words[0]=word0;
  m_values_words[1]=word1;
  m_values_words[2]=word2;
  m_values_words[3]=word3;
  
  uint32_t word;
  vector <uint32_t> cachewordsfifos_w;
    
  int counter=0;
  for(uint32_t mx = 0; mx <= 16; mx++) {
    for(uint32_t times=0; times < m_fifoSizes[mx]/32 ; times++){
      word=0;
      for(uint32_t i = 0; i <= 31; i++) {
	word=((m_fifos[mx][i+(32*times)]  & 0x1) <<  (i+(32*times)))|word;
      }
      if(verbose) {cout << "Writing FIFO ipbus reg\t[" << counter << "]\t "
			<< "the word:\t[" << setfill('0') << setw(8) << right << hex << word << "]" 
			<< dec << endl;}
      cachewordsfifos_w.push_back(word);
      m_fifos_words[counter]=word;// Add this to test dump() check--> |counter;
      counter+=1;
    }
  } 
  //IT IS NECESSARY TO ADD THE 4 MISSING WORDS TO FILL THE 256 FIFO BLOCK!! 
  cachewordsfifos_w.push_back(0);
  cachewordsfifos_w.push_back(0);
  cachewordsfifos_w.push_back(0);
  cachewordsfifos_w.push_back(0);
  //THIS TOO: Also put them in memory to force reading the full block!!
  m_fifos_words[counter]=0;
  m_fifos_words[counter+1]=0;
  m_fifos_words[counter+2]=0;
  m_fifos_words[counter+3]=0;

  if(verbose){
    //cout << "Writing to FIFO number: " <<dec <<  counter   << " the DUMMY word:\t " << hex << 0 <<endl;
    //cout << "Writing to FIFO number: " <<dec <<  counter+1 << " the DUMMY word:\t " << hex << 0 <<endl;
    //cout << "Writing to FIFO number: " <<dec <<  counter+2 << " the DUMMY word:\t " << hex << 0 <<endl;
    //cout << "Writing to FIFO number: " <<dec <<  counter+3 << " the DUMMY word:\t " << hex << 0 <<endl;
    cout << "Writing word0:\t" << hex << "0x"<< word0 << endl;
    cout << "Writing word1:\t" << hex << "0x"<< word1 << endl;
    cout << "Writing word2:\t" << hex << "0x"<< word2 << endl;
    cout << "Writing word3:\t" << hex << "0x"<< word3 << endl;
  }
  //Finally write
  m_ipb->Write(IPBADDR_MULT_1_W, word0);
  m_ipb->Write(IPBADDR_MULT_2_W, word1);
  m_ipb->Write(IPBADDR_MULT_3_W, word2);
  m_ipb->Write(IPBADDR_MULT_4_W, word3);
  m_ipb->Write(IPBADDR_FIFO_W, cachewordsfifos_w, true);
  //m_ipb->Write(93,0x40);
}


uint32_t Malta2::Read(int pos, bool force){
  if(force){
    uint32_t cacheWord;
    m_ipb->Read(pos, cacheWord);
    m_values_r[pos]=cacheWord;
  }
  return m_values_r[pos];
}


void Malta2::ReadAll(){
  if(verbose) cout << "ReadAll()" << endl;
  //Create an emtpy vector to read the configuration
  uint32_t cacheword0;
  uint32_t cacheword1;
  uint32_t cacheword2;
  uint32_t cacheword3;
  vector<uint32_t> cachewordsfifos_r;
  
  //Read slowcontrol registers 
  if(verbose) cout << "read registers" << endl;
  m_ipb->Read(IPBADDR_MULT_1_R, cacheword0); 
  m_ipb->Read(IPBADDR_MULT_2_R, cacheword1); 
  m_ipb->Read(IPBADDR_MULT_3_R, cacheword2); 
  m_ipb->Read(IPBADDR_MULT_4_R, cacheword3);
  m_ipb->Read(IPBADDR_FIFO_R, cachewordsfifos_r, 136, true);
  
  //Store output in mem
  if(verbose) cout << "fill in mem" << endl;
  m_values_words_r[0]=cacheword0;
  m_values_words_r[1]=cacheword1;
  m_values_words_r[2]=cacheword2;
  m_values_words_r[3]=cacheword3;
  for(uint32_t i=0; i < cachewordsfifos_r.size(); i++){
    m_fifos_words_r[i]=cachewordsfifos_r.at(i);
  }                                                                                                                                                                                                     
  //Put it in individual values
  if(verbose) cout << "filll the individual values" << endl;
  m_values_r[DATAFLOW_MERGERTOLEFT]  = (( cacheword0 >>   0) & 1);
  m_values_r[DATAFLOW_MERGERTORIGHT] = (( cacheword0 >>   1) & 1);
  m_values_r[DATAFLOW_LMERGERTOLVDS] = (( cacheword0 >>   2) & 1);
  m_values_r[DATAFLOW_LMERGERTOCMOS] = (( cacheword0 >>   3) & 1);
  m_values_r[DATAFLOW_RMERGERTOLVDS] = (( cacheword0 >>   4) & 1);
  m_values_r[DATAFLOW_RMERGERTOCMOS] = (( cacheword0 >>   5) & 1);
  m_values_r[DATAFLOW_LCMOS]         = (( cacheword0 >>   6) & 1 );
  m_values_r[DATAFLOW_RCMOS]         = (( cacheword0 >>   7) & 1 );
  m_values_r[DATAFLOW_ENLVDS]        = (( cacheword0 >>   8) & 1 );
  m_values_r[DATAFLOW_ENLCMOS]       = (( cacheword0 >>   9) & 1 );
  m_values_r[DATAFLOW_ENRCMOS]       = (( cacheword0 >>  10) & 1 );
  m_values_r[DATAFLOW_ENMERGER]      = (( cacheword0 >>  11) & 1 );
  m_values_r[POWER_SWITCH_LEFT]      = (( cacheword0 >>  12) & 1 );
  m_values_r[POWER_SWITCH_RIGHT]     = (( cacheword0 >>  13) & 1 );
  m_values_r[LVDS_EN]                = (( cacheword0 >>  14) & 1 );
  m_values_r[LVDS_PRE]               = (( cacheword0 >>  15) & 0xFFFF);

  m_values_r[LVDS_BRIDGE_EN]         = (( cacheword1 >>   0) & 0x1F);
  m_values_r[LVDS_CMFB_EN]           = (( cacheword1 >>   5) & 0x1F);
  m_values_r[LVDS_SET_IBCMFB]        = (( cacheword1 >>  10) & 0xF);
  m_values_r[LVDS_SET_IVPH]          = (( cacheword1 >>  14) & 0xF);
  m_values_r[LVDS_SET_IVPL]          = (( cacheword1 >>  18) & 0xF);
  m_values_r[LVDS_SET_IVNH]          = (( cacheword1 >>  22) & 0xF);
  m_values_r[LVDS_SET_IVNL]          = (( cacheword1 >>  26) & 0xF);

  m_values_r[SWCNTL_VCASN]           = (( cacheword2 >>   0) &   1);
  m_values_r[SWCNTL_VCLIP]           = (( cacheword2 >>   1) &   1);
  m_values_r[SWCNTL_VPULSE_HIGH]     = (( cacheword2 >>   2) &   1);
  m_values_r[SWCNTL_VPULSE_LOW]      = (( cacheword2 >>   3) &   1);
  m_values_r[SWCNTL_VRESET_P]        = (( cacheword2 >>   4) &   1);
  m_values_r[SWCNTL_VRESET_D]        = (( cacheword2 >>   5) &   1);
  m_values_r[SWCNTL_ICASN]           = (( cacheword2 >>   6) &   1);
  m_values_r[SWCNTL_IRESET]          = (( cacheword2 >>   7) &   1);
  m_values_r[SWCNTL_IBIAS]           = (( cacheword2 >>   8) &   1);
  m_values_r[SWCNTL_ITHR]            = (( cacheword2 >>   9) &   1);
  m_values_r[SWCNTL_IDB]             = (( cacheword2 >>  10) &   1);
  m_values_r[SWCNTL_IREF]            = (( cacheword2 >>  11) &   1);
  m_values_r[SET_IRESET_BIT]         = (( cacheword2 >>  12) &   1);
  m_values_r[SWCNTL_DACMONV]         = (( cacheword2 >>  13) &   1);
  m_values_r[SWCNTL_DACMONI]         = (( cacheword2 >>  14) &   1);
  m_values_r[SET_IBUFP_MON_0]        = (( cacheword2 >>  15) & 0xF);
  m_values_r[SET_IBUFN_MON_0]        = (( cacheword2 >>  19) & 0xF);
  m_values_r[SET_IBUFP_MON_1]        = (( cacheword2 >>  23) & 0xF);
  m_values_r[SET_IBUFN_MON_1]        = (( cacheword2 >>  27) & 0xF);

  m_values_r[PULSE_MON_L]            = (( cacheword3 >>   0) & 0x1);
  m_values_r[PULSE_MON_R]            = (( cacheword3 >>   1) & 0x1);
  m_values_r[LVDS_VBCMFB]            = (( cacheword3 >>   2) & 0xF);
  
  int counter=0;
  int bit_pos=0;
  for(uint32_t mx = 0; mx <= 16; mx++) {
    for(uint32_t times=0; times < m_fifoSizes[mx]/32 ; times++){
      for(uint32_t i = 0; i <= 31; i++) {
	bit_pos =i+(32*times);
	//cout << "m_fifos_r["<<mx<<"]["<<bit_pos<<"]= (( m_fifos_words_r["<<counter<<"] >>   i) &   1)" << endl;
	m_fifos_r[mx][bit_pos]= (( m_fifos_words_r[counter] >>   i) &   1);//FIXME
      }
      counter+=1;
    }
  }
  //Some verbose output
  if(verbose){
    cout << "R[IPBADDR_MULT_1_R] = 0x" << hex << cacheword0 << dec << endl; 
    cout << "R[IPBADDR_MULT_2_R] = 0x" << hex << cacheword1 << dec << endl; 
    cout << "R[IPBADDR_MULT_3_R] = 0x" << hex << cacheword2 << dec << endl; 
    cout << "R[IPBADDR_MULT_4_R] = 0x" << hex << cacheword3 << dec << endl; 
    for(int i=0; i < 136; i++){
      cout << "Read from FIFO \t[" << i << "]\t" 
	   <<  setfill('0') << setw(8) << right << hex << m_fifos_words_r[i] << dec << endl;
    }
  }
}

void Malta2::Send(){
  WriteAll();
}

void Malta2::Configure(){
  Send();
  SetState(0);
  SetState(1);
}

void Malta2::PrintFullSCWord(){
  //First simple words
  string tabs="\t";
  for( uint32_t a = SIMPLE_WORDS_0; a <= SIMPLE_WORDS_N-3; a++) {
    cout << left << setw(22) << names[a] << right << setw(4) << hex << m_values[a] << endl;
  }
  //FIFOs
  for(uint32_t f = 0; f <= 16; f++) {
    if (names[f]<16) tabs="\t\t";
    if (names[f]<8) tabs="\t\t\t";
    cout << left << setw(25) <<names[f];
    for(uint32_t b=0; b < m_fifoSizes[f] ; b++){
      cout << m_fifos[f][b];
    }
    cout << endl;
  }
  //Remaining simple words
  for( uint32_t a = SIMPLE_WORDS_N -3; a <= SIMPLE_WORDS_N; a++) {
    cout << left << setw(22) << names[a] << right << setw(4) << hex << m_values[a] << endl;
  }
}

int Malta2::DumpByWord(bool update){
  //Force ReadAll if update
  if(update){ReadAll();}
  
  for (uint32_t i = SIMPLE_WORDS_0 ; i <= SIMPLE_WORDS_N ; i++ ) {
    cout  << " Written " << hex << m_values[i] << "\t Read [" << m_values_r[i] << "]\t" << names[i] << endl;
  }
  for(int i = 0; i <= 16; i++) {
    cout  << "Written\t0b";
    for(uint32_t  bits=0; bits < m_fifoSizes[i] ; bits++){
      cout <<  m_fifos[i][bits];
    }
    cout << "\t" << names[i] <<  endl;

    cout  << "Read\t0b";
    for(uint32_t  bits=0; bits < m_fifoSizes[i] ; bits++){
      cout <<  m_fifos_r[i][bits];
    }
    cout << "\t" << names[i] <<  endl;
  }
  return 0;
}

int Malta2::CheckSlowControl(bool update){
  //Force ReadAll if update
  if(update){ReadAll();}
  int errors=0;
  for (uint32_t i = SIMPLE_WORDS_0 ; i <= SIMPLE_WORDS_N ; i++ ) {
    if(m_values[i]!=m_values_r[i]){errors+=1;}
  }
  for(uint32_t i = 0; i <= 16; i++) {
    //for(uint32_t  bits=0; bits < (fifos[i+1]-fifos[i]) ; bits++){
    for(uint32_t  bits=0; bits < m_fifoSizes[i] ; bits++){
      if ( m_fifos[i][bits] != m_fifos_r[i][bits] ){ errors+=1; }
    }
  }
  return errors;
}

int Malta2::DumpByIPbus(bool update){
  //Force ReadAll if update
  if(update){ReadAll();}
  
  // compare simple registers
  int error_c = 0;
  for( uint32_t a = 0; a <4; a++) {
    if (m_values_words[a]!=m_values_words_r[a]){
      if(verbose) cout << "Word num\t[" << dec << a << "]\t " <<  names[a] << " = "
		      << "\t 0x" << hex << m_values_words_r[a] << dec  
		       << "\t Expected: " << hex << m_values_words[a] << dec 
		       <<endl;
      error_c+=1;
    }
  }
  // compare fifo words
  for( uint32_t a = 0; a <136; a++) {
    if (m_fifos_words[a]!=m_fifos_words_r[a]){
      //MISMATCH
      if(verbose) cout << "Reg num\t[" << dec << a << "]\t =" 
		       << "\t 0x" << hex << m_fifos_words_r[a] << dec
		       << "\t Expected: " << hex << m_fifos_words[a] << dec 
		       << endl;
      error_c+=1;
    }
  }
  cout << "Dump(" << std::boolalpha << update << ") Number ipbus registers with errors: " << dec <<  error_c << endl;
  return error_c;
}


void Malta2::SetState(int val){
  m_ipb->Write(IPBADDR_SCCONTROL,val);
}

uint32_t Malta2::Word(std::string word){
  return addresses[word];
}

void Malta2::thermomiter_encoder(int pos, uint32_t  value){
  div_t vv;
  for(int i=0; i<128; i++){ 
    m_fifos[pos][i]=0;
  }
  vv=std::div(value,2);
  for (int i= 63-vv.quot; i<63; i++){ m_fifos[pos][i]=1;}
  for (int i=63; i<63+vv.quot+vv.rem; i++){ m_fifos[pos][i]=1;}
  if(verbose){
    cout << "Thermometer encoding " << value << " to  => 0b" ;
    for (int i=0; i<128; i++){
      cout <<  m_fifos[pos][i] ;  
    }
    cout << endl;
  }
}

void Malta2::hot_encoder(int pos, uint32_t  value){
  if(value>127) value=127;
  value=127-value;
  int size_of_fifo=m_fifoSizes[pos];
  for(int i = 0; i < size_of_fifo; i++) {
    m_fifos[pos][i]=0;
  }
  m_fifos[pos][value]=1;
}

void Malta2::initialize_array(){
  for(uint32_t  mx = FIFO_WORDS_0; mx <=FIFO_WORDS_N; mx++) {
    uint32_t size_of_fifo=(fifos[mx+1]-fifos[mx]);
    m_fifoSizes[mx]=size_of_fifo;
    for(uint32_t i = 0; i < size_of_fifo; i++) {
      defsFIFO[mx][i]=0;    
    }
  }
}


bool Malta2::SetConfigFromFile(const std::string& fileName){

  ifstream fr;
  cout << "Malta2::SetConfigFromFile " << fileName << endl;
  fr.open(fileName.c_str());
  if (!fr.is_open()){
    cout << "#####################################################"<<endl;
    cout << "##  The configuration file cannot be opened        ##" << endl;
    cout << "#####################################################"<<endl;
    return false;
  }
  while (fr.good()){
    
    string buffer;
    getline(fr, buffer);
    
    // Ignore if is a comment
    size_t end = buffer.find_first_of('#');
    if (end==0) continue;
    
    // Ignore if line is empty
    if(buffer.length()==0) continue;
    cout << buffer << endl; 
    // Configure MALTA
    if(!SetConfigFromString(buffer)){
      return false;
    }
    
  }
  fr.close();
  return true;
}

bool Malta2::SetConfigFromString(const std::string& str){
  
  vector<std::string> tokens;
  //std::string delimiters = " :=\n\r,;.-";
  std::string delimiters = ":;=\n\r";
 
  // Skip delimiters at beginning.
  std::string::size_type lastPos = str.find_first_not_of(delimiters, 0);
  // Find first "non-delimiter".
  std::string::size_type pos     = str.find_first_of(delimiters, lastPos);
  
  while (std::string::npos != pos || std::string::npos != lastPos){
    // Found a token, add it to the vector.
    tokens.push_back(str.substr(lastPos, pos - lastPos));
    // Skip delimiters.  Note the "not_of"
    lastPos = str.find_first_not_of(delimiters, pos);
    // Find next "non-delimiter"
    pos = str.find_first_of(delimiters, lastPos);
  }
  
  for(uint i=0; i<tokens.size();i+=2){
    try{
      string command=tokens.at(i);
      uint32_t value=0;
      std::string value_s;
      vector<uint32_t> values;
      if(tokens.at(i+1).find("0x") != std::string::npos){
	value = std::stoi(tokens.at(i+1), 0, 16);
      }else if(  tokens.at(i+1).find("true") != string::npos ||
		 tokens.at(i+1).find("True") != string::npos ||
		 tokens.at(i+1).find("TRUE") != string::npos
		 ){
	value = 1;
      }else if(  tokens.at(i+1).find("false") != string::npos ||
		 tokens.at(i+1).find("False") != string::npos ||
		 tokens.at(i+1).find("FALSE") != string::npos
		 ){
	value = 0;
      }else if(tokens.at(i+1).find(",") != string::npos ){
	istringstream ss(tokens.at(i+1));
	string token;
	while(getline(ss, token, ',')){
	  value = std::stoi(token);
	  values.push_back(value);
	}
      }else if(tokens.at(i+1).find("txt") != string::npos){
        value_s = tokens.at(i+1);
	value_s.erase(value_s.find_last_not_of(" \f\n\r\t\v") + 1 );
	value_s.erase(0, value_s.find_first_not_of(" \f\n\r\t\v"));
      }else{
	value = std::stoi(tokens.at(i+1));
      }

      if(verbose){
	cout << "Command: " << command << endl;
	cout << "Value: " << value;
	for(uint32_t i=0;i<values.size();i++){
	  cout << " " << values.at(i);
	}
	cout << endl;
      }

      if     (command==names[SWCNTL_IDB])            { EnableIDB(value); }
      else if(command==names[SWCNTL_DACMONI])        { EnableMonDacCurrent(value); }
      else if(command==names[SWCNTL_DACMONV])        { EnableMonDacVoltage(value); }
      else if(command==names[SWCNTL_ITHR])           { EnableITHR(value); }
      else if(command==names[SWCNTL_IBIAS])          { EnableIBIAS(value); }
      else if(command==names[SWCNTL_IRESET])         { EnableIRESET(value); }
      else if(command==names[SWCNTL_ICASN])          { EnableICASN(value); }
      else if(command==names[SWCNTL_VCASN])          { EnableVCASN(value); }
      else if(command==names[SWCNTL_VCLIP])          { EnableVCLIP(value); }
      else if(command==names[SWCNTL_VPULSE_HIGH])    { EnableVPULSE_HIGH(value); }
      else if(command==names[SWCNTL_VPULSE_LOW])     { EnableVPULSE_LOW(value); }
      else if(command==names[SWCNTL_VRESET_D])       { EnableVRESET_D(value); }
      else if(command==names[SWCNTL_VRESET_P])       { EnableVRESET_P(value); }
      else if(command==names[SET_IDB])               { SetIDB(value); }
      else if(command==names[SET_ITHR])              { SetITHR(value); }
      else if(command==names[SET_IBIAS])             { SetIBIAS(value); }
      else if(command==names[SET_IRESET])            { SetIRESET(value); }
      else if(command==names[SET_ICASN])             { SetICASN(value); }
      else if(command==names[SET_VRESET_D])          { SetVRESET_D(value); }
      else if(command==names[SET_VRESET_P])          { SetVRESET_P(value); }
      else if(command==names[SET_VCASN])             { SetVCASN(value); }
      else if(command==names[SET_VCLIP])             { SetVCLIP(value); }
      else if(command==names[SET_VPULSE_HIGH])       { SetVPULSE_HIGH(value); }
      else if(command==names[SET_VPULSE_LOW])        { SetVPULSE_LOW(value); }


      //// !!!!!! check true and false statements
      else if(command==names[MASK_FULLCOL])          { SetDoubleColumnMask(value,true);}   
      else if(command==names[MASK_DOUBLE_COLUMNS])   { SetDoubleColumnMaskRange(values[0], values[1],true);}
      else if(command==names[MASK_COL])		     { SetPixelMaskColumn(value,true);}
      else if(command==names[MASK_HOR])		     { SetPixelMaskRow(value,true);}
      else if(command==names[MASK_PIXEL])	     { SetPixelMask(values[0], values[1],true);} 
      else if(command==names[MASK_PIXELS_FROM_TXT])  { SetFullPixelMaskFromFile(value_s);}
      else if(command==names[ROI])                   { SetROI(values[0], values[1], values[2], values[3],false);}
      else if(command==names[TAP_FROM_TXT])          { ReadTapsFromFile(value_s);} 
      else if(command==names[PULSE_HOR])             { SetPixelPulseRow(value,true);}
      else if(command==names[PULSE_COL])             { SetPixelPulseColumn(value,true);}
      //UNPULSE_COL/HOR not available
      
      else{ cout << "Command not found: " << command << endl; } 
    }catch(std::invalid_argument& ia){
      cout << "Malta2::SetConfigFromTextFile: "
	   << "Unable to parse [" << tokens.at(i) << " " << tokens.at(i+1) << "]" << endl;
      return false;
    }
   
  }
  return true;
}

void Malta2::ReadMaltaWord(uint32_t * values, uint32_t numwords){
  m_ipb->Read(IPBADDR_AOFIFO,values,numwords,true);
}

void Malta2::ReadMonitorWord(uint32_t * values) {
  m_ipb->Read(7,values,37,true);
}

void Malta2::Sync(){
  m_ipb->Sync();
}

uint32_t Malta2::ReadFifoStatus(){
  m_ipb->Read(IPBADDR_BUSY, m_busy);
  return m_busy;
}

bool Malta2::IsFifo1Full(){
  return (m_busy & FIFO1_FULL)!=0;
}

bool Malta2::IsFifo1Empty(){
  return (m_busy & FIFO1_EMPTY)==0;
}

bool Malta2::IsFifo1Half(){
  return (m_busy & FIFO1_HALF)==0;
}

bool Malta2::IsFifo2Full(){
  return (m_busy & FIFO2_FULL)!=0;
}

bool Malta2::IsFifo2Empty(){
  return (m_busy & FIFO2_EMPTY)==0;
}

bool Malta2::IsFifo2Half(){
  return (m_busy & FIFO2_HALF)==0;
}

bool Malta2::IsFifoMonEmpty(){
  return (m_busy & FIFOM_EMPTY)==0;
}

void Malta2::ResetFifo(){
  uint32_t cacheWord;
  m_ipb->Read(0x8,cacheWord);
  vector<uint32_t> commands;
  uint32_t maskFIFO   =0xFFFFFFFF ^ (1<<2);
  uint32_t maskOSERDES=0xFFFFFFFF ^ (1<<3);
  commands.push_back(cacheWord | (1<<2) );
  commands.push_back(cacheWord & maskFIFO );
  commands.push_back(cacheWord | (1<<3) );
  commands.push_back(cacheWord & maskOSERDES );
  m_ipb->Write(IPBADDR_DUMCTR,commands,true);
}

void Malta2::EnableMerger(bool enable){
  cout << "EnableMerger: Im not doing anything...." << endl;
}

void Malta2::EnableMonDacCurrent(bool enable){
  m_values[SWCNTL_DACMONI]=enable;
}

void Malta2::EnableMonDacVoltage(bool enable){
  m_values[SWCNTL_DACMONV]=enable;
}

void Malta2::EnableIDB(bool enable){
  m_values[SWCNTL_IDB]=enable;
}

void Malta2::EnableITHR(bool enable){
  m_values[SWCNTL_ITHR]=enable;
}

void Malta2::EnableIBIAS(bool enable){
  m_values[SWCNTL_IBIAS]=enable;
}

void Malta2::EnableIRESET(bool enable){
  m_values[SWCNTL_IRESET]=enable;
}

void Malta2::EnableICASN(bool enable){
  m_values[SWCNTL_ICASN]=enable;
}

void Malta2::EnableVCASN(bool enable){
  m_values[SWCNTL_VCASN]=enable;
}

void Malta2::EnableVCLIP(bool enable){
  m_values[SWCNTL_VCLIP]=enable;
}

void Malta2::EnableVPULSE_HIGH(bool enable){
  m_values[SWCNTL_VPULSE_HIGH]=enable;
}

void Malta2::EnableVPULSE_LOW(bool enable){
  m_values[SWCNTL_VPULSE_LOW]=enable;
}

void Malta2::EnableVRESET_D(bool enable){
  m_values[SWCNTL_VRESET_D]=enable;
}

void Malta2::EnableVRESET_P(bool enable){
  m_values[SWCNTL_VRESET_P]=enable;
}

void Malta2::SetIDB(uint32_t value){
  Write(SET_IDB, value);
}

void Malta2::SetITHR(uint32_t value){
  Write(SET_ITHR, value);
}

void Malta2::SetIBIAS(uint32_t value){
  Write(SET_IBIAS, value);
}

void Malta2::SetIRESET(uint32_t value){
  Write(SET_IRESET, value);
}

void Malta2::SetICASN(uint32_t value){
  Write(SET_ICASN, value);
}

void Malta2::SetVCASN(uint32_t value){
  Write(SET_VCASN, value);
}

void Malta2::SetVCLIP(uint32_t value){
  Write(SET_VCLIP, value);
}

void Malta2::SetVPULSE_HIGH(uint32_t value){
  Write(SET_VPULSE_HIGH, value);
}

void Malta2::SetVPULSE_LOW(uint32_t value){
  Write(SET_VPULSE_LOW, value);
}

void Malta2::SetVRESET_D(uint32_t value){
  Write(SET_VRESET_D, value);
}

void Malta2::SetVRESET_P(uint32_t value){
  Write(SET_VRESET_P, value);
}

void Malta2::SetLVDS_VBCMFB(uint32_t value){
  Write(LVDS_VBCMFB, value);
}

void Malta2::SetPulseWidth(uint32_t value){
  if(value==2000){Write(LVDS_VBCMFB, 0xE);}
  if(value==1000){Write(LVDS_VBCMFB, 0xD);}
  if(value== 750){Write(LVDS_VBCMFB, 0xB);}
  if(value== 500){Write(LVDS_VBCMFB, 0x7);}  
}

uint32_t Malta2::GetIDB(){
  return 0; //fixme
}

uint32_t Malta2::GetITHR(){
  return 0;
}

uint32_t Malta2::GetIBIAS(){
  return 0;
}

uint32_t Malta2::GetIRESET(){
  return 0;
}

uint32_t Malta2::GetICASN(){
  return 0;
}

uint32_t Malta2::GetVCASN(){
  return 0;
}

uint32_t Malta2::GetVCLIP(){
  return 0;
}

uint32_t Malta2::GetVPULSE_HIGH(){
  return 0;
}

uint32_t Malta2::GetVPULSE_LOW(){
  return 0;
}

uint32_t Malta2::GetVRESET_D(){
  return 0;
}

uint32_t Malta2::GetVRESET_P(){
  return 0;
}

uint32_t Malta2::GetLVDS_VBCMFB(){
  return 0;
}

uint32_t Malta2::GetPulseWidth(){
  return 0;
}

void Malta2::WriteConstDelays(uint32_t delay1, uint32_t delay2){
  for(uint32_t bit=0; bit<38; bit++){
    WriteTap(bit, delay1, delay2);
  }
}

void Malta2::WriteTap(uint32_t bit, uint32_t tap1, uint32_t tap2){
  if(verbose)
    if (bit<23 or (bit>26 and bit<34)) { 
      cout << "Malta2::WriteTap "
	   << "bit: " << setw(2) << bit << ", "
	   << "addr: " << setw(2) << IPBADDR_TAP0+bit << ", "
	   << "tap1: " << setw(2) << tap1 << ", "
	   << "tap2: " << setw(2) << tap2 << endl;
    }
  uint32_t values[2];
  values[0]=((tap2&0x1F) << 5) | (tap1 & 0x1F);
  values[0]+=pow(2,31);
  values[1]=0;
  m_ipb->Write(IPBADDR_TAP0+bit,values,2,true); //FIFO    
}

vector<uint32_t> Malta2::ReadTap(uint32_t bit){
  if(bit>=NTAPS)bit=NTAPS-1;
  uint32_t value;
  m_ipb->Read(IPBADDR_TAP0_R+bit,value);
  uint32_t tap1=((value>>0) & (0x1F));
  uint32_t tap2=((value>>5) & (0x1F));
  if(verbose){
    if (bit<23 or (bit>26 and bit<34)) { 
      cout << "Malta2::ReadTap  "
	   << "bit: " << setw(2) << bit << ", "
	   << "addr: " << setw(2) << IPBADDR_TAP0_R+bit << ", "
	   << "tap1: " << setw(2) << tap1 << ", "
	   << "tap2: " << setw(2) << tap2 << endl;
    }
  }
  vector<uint32_t> ret;
  ret.push_back(tap1);
  ret.push_back(tap2);
  return ret;
}

void Malta2::ReadTapsFromFile(string fileName){
  cout << "Malta2::ReadTapsFromFile " << endl;
  std::ifstream myfile(fileName);
  if(myfile.fail()){
    cout << "File: "<<fileName<< "does not exist, check the path"<<endl;
    exit(0);
    return;
  }
  
  int bit, tap1, tap2;
  while ( myfile >> bit >> tap1 >> tap2){
    WriteTap(bit,tap1,tap2);
  }
  myfile.close();
}

void Malta2::WriteTapsToFile(string fileName){
  cout << "Malta2::WriteTapsToFile " << endl;
  ofstream myfile(fileName,ofstream::trunc);
  if(myfile.fail()){
    cout << "Cannot open file for writing: "<<fileName<<endl;
    return;
  }
  
  for(uint32_t bit=0;bit<NTAPS;bit++){
    vector<uint32_t> taps=ReadTap(bit);
    if(taps.size()==0) continue;
    myfile << bit << "\t" << taps.at(0) << "\t" << taps.at(1) << endl;
  }
  myfile.close();
}

void Malta2::Trigger(uint32_t ntimes, bool withPulse){
  uint32_t value;
  m_ipb->Read(IPBADDR_DUMCTR,value);
  //VD: remember: max command is 250!!!
  vector<uint32_t> commands;
  if (withPulse) { 
    
    unsigned int Multiple=15;//8;
    ntimes=ntimes/Multiple;
    for(uint32_t i=0; i<Multiple; i++) {
      commands.push_back(CTRL_PULSE | CTRL_INCTRIG);
      for(uint32_t i=0;i<10;i++) { commands.push_back((value) & CTRL_PULSE);} //20
      for(uint32_t i=0;i<5; i++) { commands.push_back(0); }                   //10
    }   
  
  } else {
    
    commands.push_back( value | CTRL_INCTRIG);
    for (unsigned int i=0; i<20; i++) { commands.push_back( (value) & ~CTRL_INCTRIG); }     
    for (unsigned int i=0; i<4; i++) {
      commands.push_back( value | CTRL_INCTRIG);
      for (unsigned int j=0; j<20; j++) { commands.push_back( (value) & ~CTRL_INCTRIG); }
    }
  }
  
  //cout << "SIZE OF THE vector is: " << commands.size() << " for a number of Trigger: " << ntimes << endl;
  for (unsigned int iC=0; iC<ntimes; iC++) {
    m_ipb->Write(IPBADDR_DUMCTR,commands,true);
  }
}

void Malta2::SetReadoutDelay(uint32_t delay){
  if(delay > 255) delay = 255;
  m_ipb->Write(AO_ADDR_RODELAY,delay);
}

uint32_t Malta2::GetReadoutDelay(){
  uint32_t delay = 0;
  m_ipb->Read(AO_ADDR_RODELAY,delay);
  return delay;
}

void Malta2::SetPixelPulse(uint32_t col, uint32_t row, bool enable){ 
  SetPixelPulseRow(row, enable);
  SetPixelPulseColumn(col, enable);
}

void Malta2::SetPixelPulseRow(uint32_t row, bool enable){
  if(row>511) row=511;
  m_fifos[PULSE_HOR][511-row]=enable;
}

void Malta2::SetPixelPulseColumn(uint32_t col, bool enable){
  if(col>511) col=511;
  m_fifos[PULSE_COL][511-col]=enable;
}

void Malta2::SetPixelMask(uint32_t col, uint32_t row, bool enable){
  if(col>511) col=511;
  if(row>511) row=511;
  m_fifos[MASK_COL][511-col]=!enable;
  m_fifos[MASK_HOR][511-row]=!enable;
  m_fifos[MASK_DIAG][511-get_diagonal(col,row)]=!enable;
  m_mask_col[col]=enable;
  m_mask_row[row]=enable;
  m_mask_diag[get_diagonal(col,row)]=enable;

  //adding masked pixels in TH2I
  std::pair<uint32_t,uint32_t> pixel(col,row);
  m_maskedPixelList.push_back(pixel);
}

void Malta2::SetPixelMaskRow(uint32_t row, bool enable){
  if(row>511) row=511;
  m_fifos[MASK_HOR][511-row]=!enable;
  m_mask_row[row]=enable;
}

void Malta2::SetPixelMaskColumn(uint32_t col, bool enable){
  if(col>511) col=511;
  m_fifos[MASK_COL][511-col]=!enable;
  m_mask_col[col]=enable;
}

void Malta2::SetPixelMaskDiag(uint32_t diag, bool enable){
  if(diag>511) diag=511;
  m_fifos[MASK_DIAG][511-diag]=!enable;
  m_mask_diag[diag]=enable;
}

void Malta2::SetDoubleColumnMask(uint32_t dc, bool enable){
  if(dc>255) dc=255;
  m_fifos[MASK_FULLCOL][255-dc]=!enable;
  m_mask_dc[dc]=enable;
}

void Malta2::SetDoubleColumnMaskRange(uint32_t dc1, uint32_t dc2, bool enable){
  if(dc1>255) dc1=255;
  cout << dc2 << endl;
  if(dc2>255) dc2=255;
  for(uint32_t dc=dc1; dc<=dc2; dc++){
    m_fifos[MASK_FULLCOL][255-dc]=!enable;
    m_mask_dc[dc]=enable;
  }
}

void Malta2::SetFullPixelMaskFromFile(std::string fileName){

  fileName.erase( fileName.find_last_not_of(" \f\n\r\t\v")+1);
  fileName.erase(0, fileName.find_first_not_of(" \f\n\r\t\v"));

  std::vector<int> xpair, ypair;

  //reading from file; check if root or txt file exists
  if(fileName.find(".txt")!=string::npos){ 
    int ppx, ppy;
    std::ifstream myfile(fileName);
    if(myfile.fail()){
      cout << "File: "<<fileName<< "does not exist, check the path"<<endl;
      return;
    }
    while ( myfile >> ppx >> ppy){
      xpair.push_back(ppx);
      ypair.push_back(ppy);
    }
    myfile.close();
    cout << "Masking from TXT file; Total number of masked pixels is: " << xpair.size() << endl;
  }
  
  else if(fileName.find(".root")!=string::npos){
    TFile fr(fileName.c_str(), "READ");
    if(fr.IsZombie()){
      cout << "root file is corrupted or does not exist" << endl;
      return;
    }
    std::string noisemap="NoiseMap";
    if(!fr.GetListOfKeys()->Contains(noisemap.c_str())){
      cout << "histogram with name "<<noisemap<<" does not exists in the root file" << endl;
      return;
    }
    TH2I* h2 = (TH2I*)fr.Get(noisemap.c_str());

    for(int32_t i=0;i<h2->GetNbinsX();i++){
      for(int32_t j=0;j<h2->GetNbinsY();j++){
        uint32_t binn=h2->GetBin(i,j);
        if(h2->GetBinContent(binn)>0){
          xpair.push_back(i);
          ypair.push_back(j);
        }
      }
    }
    cout << "Masking from root file; Total number of masked pixels is: " << xpair.size() << endl;
  }

  else{
    cout << "Unrecognized file extension..." << endl;
    return;
  }


  if(xpair.size() != ypair.size()){
    cout << "Size of the passed pixel arrays for masking do not match!!!!" <<endl;
    return;
  }

  // actual masking
  for(uint32_t p=0; p<xpair.size();p++){
    int col = xpair.at(p);
    int row = ypair.at(p);

    // check if the pixel is not already in the maskedPixelList
    bool doExist = false;
    for(auto pix: m_maskedPixelList){
      int x = pix.first;
      int y = pix.second;
      if (x == col && y == row) doExist = true;
    }
    if (doExist) continue;

    //mask pixel
    cout << "  [ " << col << " , " << row << " ]" << endl;
    SetPixelMask(col,row,true);
    std::pair<uint32_t,uint32_t> pixel(col,row); 
    m_maskedPixelList.push_back(pixel);
  }
}

void Malta2::SetROI(uint32_t col1, uint32_t row1, uint32_t col2, uint32_t row2, bool mask){
  cout << "Malta2::SetROI " << col1 << ", " << row1 << ", " << col2 << ", " << row2 << endl;
  if(col2<=col1){
    cout << "Malta2::SetROI range error:" << " x1:" << col1 << " x2:" << col2 << endl;
    return;
  }
  if(row2<=row1){
    cout << "Malta2::SetROI range error:" << " y1:" << row1 << " y2:" << row2 << endl;
    return;
  }

  cout << col1 << endl;
  cout << col2 << endl;
  cout << row1 << endl;
  cout << row2 << endl;
  if(col1>511) col1=511;
  if(col2>511) col2=511;
  if(row1>511) row1=511;
  if(row2>511) row2=511;
  uint32_t dc1 = (col1>1)?(col1>>1):0;
  uint32_t dc2 = (col2>1)?(col2>>1):0;
  
  //Mask double columns from 0 to dc1
  cout << "Malta2::SetROI masking double columns from " << 0 << " to " << ((dc1>0)?(dc1-1):0) << endl;
  if(dc1>0) SetDoubleColumnMaskRange(0,dc1-1,mask);
  //Mask double columns from dc2 to 255
  cout << "Malta2::SetROI masking double columns from " << ((dc2<255)?(dc2+1):255) << " to " << 255 << endl;
  if(dc2<255) SetDoubleColumnMaskRange(dc2+1,255,mask);
  //Mask col from col1 to col2
  cout << "Malta2::SetROI masking columns from " << col1 << " to " << col2 << endl;
  for(uint32_t col=col1; col<=col2; col++){SetPixelMaskColumn(col,mask);}

  //Mask row from 0 to row1
  cout << "Malta2::SetROI masking rows from " << 0 << " to " << row1 << endl;
  for(uint32_t row=0; row<row1; row++){SetPixelMaskRow(row,mask);}

  //Mask row from row2 to 511
  cout << "Malta2::SetROI masking rows from " << (row2+1) << " to " << 511 << endl;
  for(uint32_t row=row2+1; row<=511; row++){SetPixelMaskRow(row,mask);}

  //Mask diag from 0 to 511
  cout << "Malta2::SetROI masking all diagonals " << 0 << " to " << 511 << endl;
  for(uint32_t diag=0; diag<=511; diag++){SetPixelMaskDiag(diag,mask);}
}

uint32_t Malta2::get_diagonal(int col, int row){
  int diag = row-col;
  if(diag < 0) diag += 512;
  return diag;
}

void Malta2::SetExternalL1A(bool enable){
  uint32_t cacheWord;
  m_ipb->Read(IPBADDR_DUMCTR,cacheWord);
  if(enable) cacheWord |= CTRL_L1A_EXT;
  else cacheWord &= (~CTRL_L1A_EXT);
  m_ipb->Write(IPBADDR_DUMCTR, cacheWord);
  //REPLACE THE PREVIOUS WITH THIS
  //m_ipb->RMWB(IBADDR_DUMCTR,~CTRL_L1A_EXT,(enable?CTRL_L1A_EXT:0));
}

void Malta2::ResetL1Counter(){
  uint32_t cacheWord;
  m_ipb->Read(IPBADDR_DUMCTR,cacheWord);
  m_ipb->Write(IPBADDR_DUMCTR, cacheWord | CTRL_L1A_RST );
  //REPLACE THE PREVIOUS WITH THIS
  //m_ipb->RMWB(IBADDR_DUMCTR,~CTRL_L1_RST,CTRL_L1A_RST);
  
  //usleep(1000); // MLB

  uint32_t maskRESET= 0xFFFFFFFF ^ CTRL_L1A_RST;
  m_ipb->Write(IPBADDR_DUMCTR, cacheWord & maskRESET );
  //REPLACE THE PREVIOUS WITH THIS
  //m_ipb->RMWB(IBADDR_DUMCTR,~CTRL_L1_RST,0);

  //usleep(1000); // MLB
}

void Malta2::ReadoutOn(){
  uint32_t cacheWord;
  m_ipb->Read(IPBADDR_DUMCTR,cacheWord);
  uint32_t mask=0xFFFFFFFF ^ CTRL_RO_OFF;
  m_ipb->Write(IPBADDR_DUMCTR, cacheWord & mask);
  //REPLACE THE PREVIOUS WITH THIS
  //m_ipb->RMWB(IBADDR_DUMCTR,~CTRL_RO_OFF,0);
}

void Malta2::ReadoutOff(){
  uint32_t cacheWord;
  m_ipb->Read(IPBADDR_DUMCTR,cacheWord);
  m_ipb->Write(IPBADDR_DUMCTR, cacheWord | CTRL_RO_OFF);
  //REPLACE THE PREVIOUS WITH THIS
  //m_ipb->RMWB(IBADDR_DUMCTR,~CTRL_RO_OFF,CTRL_RO_OFF);
}

void Malta2::DisableFastSignal(){
  SetFastSignal(false);
}

void Malta2::EnableFastSignal(){
  SetFastSignal(true);
}

void Malta2::SetFastSignal(bool enable){
  uint32_t cacheWord;
  m_ipb->Read(IPBADDR_DELAY,cacheWord);
  if(enable) cacheWord |= DELAY_FAST_EN;
  else cacheWord &= (~DELAY_FAST_EN);
  m_ipb->Write(IPBADDR_DELAY, cacheWord);
  //REPLACE THE PREVIOUS WITH THIS
  //m_ipb->RMWB(IBADDR_DELAY,~DELAY_FAST_EN,(enable?DELAY_FAST_EN:0));
}

uint32_t Malta2::GetL1ID(){
  uint32_t cacheWord;
  m_ipb->Read(IPBADDR_BUSY,cacheWord);
  return ((cacheWord & L1A_COUNTER) >> 20);
}

uint32_t Malta2::GetFIFO2WordCount(){
  uint32_t cacheWord;
  m_ipb->Read(IPBADDR_WDCTR,cacheWord);
  if (cacheWord%2!=0 and cacheWord >0) cacheWord = cacheWord-1;
  return cacheWord;
}

uint32_t Malta2::GetNumCalls(){
  return m_ipb->GetNumCalls();
}

void Malta2::SetCapacitance(float capacitance){
  m_capacitance = capacitance;
}

float Malta2::GetCapacitance(){
  return m_capacitance;
}
