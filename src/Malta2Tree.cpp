#include "Malta2/Malta2Tree.h"

using namespace std;

Malta2Tree::Malta2Tree(){
  pixel=0;
  group=0;
  parity=0;
  delay=0;
  dcolumn=0;
  chipbcid=0;
  chipid=0;
  phase=0;
  winid=0;
  bcid=0;
  run=0;
  l1id=0;
  l1idC=0;
  isDuplicate=0;
  timer=0;
  idb=0;
  ithr=0;
  vlow=0;
  vhigh=0;
  m_entry=0;
  m_file=0;
  m_readonly=false;
  m_extended=false;
}

Malta2Tree::~Malta2Tree(){
  if(m_file) delete m_file;
}

void Malta2Tree::Open(string filename, string options){
  m_file = TFile::Open(filename.c_str(),options.c_str());
  if(options=="READ"){
    m_readonly=true;
    m_tree = (TTree*)m_file->Get("MALTA"); //Should replace by MALTA2
    m_tree->SetBranchAddress("pixel", &pixel);
    m_tree->SetBranchAddress("group", &group);
    m_tree->SetBranchAddress("parity", &parity);
    m_tree->SetBranchAddress("delay", &delay);
    m_tree->SetBranchAddress("dcolumn", &dcolumn);
    m_tree->SetBranchAddress("chipbcid", &chipbcid);
    m_tree->SetBranchAddress("chipid", &chipid);
    m_tree->SetBranchAddress("phase", &phase);
    m_tree->SetBranchAddress("winid", &winid);
    m_tree->SetBranchAddress("bcid", &bcid);
    m_tree->SetBranchAddress("runNumber", &run);
    m_tree->SetBranchAddress("l1id", &l1id);
    m_tree->SetBranchAddress("l1idC", &l1idC);
    m_tree->SetBranchAddress("isDuplicate", &isDuplicate);
    m_tree->SetBranchAddress("timer", &timer);    
  }else{
    m_tree = new TTree("MALTA","MALTA2 Data"); //Should replace by MALTA2
    m_tree->Branch("pixel",&pixel,"pixel/i");
    m_tree->Branch("group",&group,"group/i");
    m_tree->Branch("parity",&parity,"parity/i");
    m_tree->Branch("delay",&delay,"delay/i");
    m_tree->Branch("dcolumn",&dcolumn,"dcolumn/i");
    m_tree->Branch("chipbcid",&chipbcid,"chipbcid/i");
    m_tree->Branch("chipid",&chipid,"chipid/i");
    m_tree->Branch("phase",&phase,"phase/i");
    m_tree->Branch("winid",&winid,"winid/i");
    m_tree->Branch("bcid",&bcid,"bcid/i");
    m_tree->Branch("runNumber",&run,"runNumber/i");
    m_tree->Branch("l1id",&l1id,"l1id/i");
    m_tree->Branch("l1idC",&l1idC,"l1idC/i"); //Valerio
    m_tree->Branch("isDuplicate",&isDuplicate,"isDuplicate/i");
    m_tree->Branch("timer",&timer,"timer/f");
    m_t0 = chrono::steady_clock::now();
  }
  m_entry = 0;
}

void Malta2Tree::Extend(){
  if(!m_extended){
    if(m_readonly){
      m_tree->SetBranchAddress("idb", &idb);
      m_tree->SetBranchAddress("ithr", &ithr);
      m_tree->SetBranchAddress("vhigh", &vhigh);
      m_tree->SetBranchAddress("vlow", &vlow);
    }else{
      m_tree->Branch("idb",&idb,"idb/i");
      m_tree->Branch("ithr",&ithr,"ithr/i");
      m_tree->Branch("vhigh",&vhigh,"vhigh/i");
      m_tree->Branch("vlow",&vlow,"vlow/i"); 
    }
    m_extended=true;
  }
}

void Malta2Tree::Set(Malta2Data * data){
  pixel = data->getPixel();
  group = data->getGroup();
  parity = data->getParity();
  delay = data->getDelay();
  dcolumn = data->getDcolumn();
  chipbcid = data->getChipbcid();
  chipid = data->getChipid();
  phase = data->getPhase();
  winid = data->getWinid();
  bcid = data->getBcid();
  l1id = data->getL1id();
  timer = chrono::duration_cast<chrono::milliseconds>(chrono::steady_clock::now()-m_t0).count()/1000.;
}

void Malta2Tree::SetRunNumber(uint32_t v){
  run = v;
}

void Malta2Tree::SetL1idC(uint32_t v){
  l1idC = v;
}

void Malta2Tree::SetIsDuplicate(uint32_t v){
  isDuplicate = v;
}

void Malta2Tree::SetIDB(uint32_t v){
  idb = v;
}

void Malta2Tree::SetITHR(uint32_t v){
  ithr = v;
}

void Malta2Tree::SetVLOW(uint32_t v){
  vlow = v;
}

void Malta2Tree::SetVHIGH(uint32_t v){
  vhigh = v;
}

uint32_t Malta2Tree::GetRunNumber(){
  return run;
}

uint32_t Malta2Tree::GetL1idC(){
  return l1idC;
}

uint32_t Malta2Tree::GetIsDuplicate(){
  return isDuplicate;
}

uint32_t Malta2Tree::GetIDB(){
  return idb;
}

uint32_t Malta2Tree::GetITHR(){
  return ithr;
}

uint32_t Malta2Tree::GetVLOW(){
  return vlow;
}

uint32_t Malta2Tree::GetVHIGH(){
  return vhigh;
}

float Malta2Tree::GetTimer(){
  return timer;
}

void Malta2Tree::Fill(){
  m_tree->Fill();
}

int Malta2Tree::Next(){
  if(m_tree->LoadTree(m_entry)<0) return 0;
  m_tree->GetEntry(m_entry);
  m_entry++;
  return m_entry;
}

TFile* Malta2Tree::GetFile(){
  return m_file;
}

Malta2Data * Malta2Tree::Get(){
  m_data.setPixel(pixel);
  m_data.setGroup(group);
  m_data.setParity(parity);
  m_data.setDelay(delay);
  m_data.setDcolumn(dcolumn);
  m_data.setChipbcid(chipbcid);
  m_data.setChipid(chipid);
  m_data.setPhase(phase);
  m_data.setWinid(winid);
  m_data.setBcid(bcid);
  m_data.setL1id(l1id);
  m_data.pack();
  m_data.unpack();
  return &m_data;
}

void Malta2Tree::Close(){
  if(!m_readonly){
    m_file->cd();
    m_tree->Write();
  }
  m_file->Close();
}
