//! -*-C++-*-
#include "Malta2/Malta2.h"
#include "Malta2/Malta2Utils.h"
#include "Malta2/Malta2Masker.h"
#include <cmdl/cmdargs.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <signal.h>
#include <iomanip>
#include <time.h>
#include <unistd.h>
#include <sys/stat.h>

#include "TCanvas.h"
#include "TH2I.h"
#include "TLine.h"
#include "TStyle.h"
#include "TFile.h"
#include "TTree.h"
#include "TROOT.h"

using namespace std;

int main(int argc, char *argv[]){

  cout << "#####################################" << endl
       << "# Welcome to Malta2 tap calibration #" << endl
       << "#####################################" << endl;

  CmdArgBool   cVerbose ( 'v',"verbose","turn on verbose mode");
  CmdArgStr    cAddress ( 'a',"address","address","connection string udp://ip:port.",CmdArg::isREQ);
  CmdArgStr    cChip    ( 'c',"chip","sample","WXRX",CmdArg::isREQ);
  CmdArgStr    cTag     ( 't',"tag","text","test description. Default test");
  CmdArgInt    cNpulses ( 'n',"npulses","npulses","number of pulses. Default 100");
  CmdArgStr    cInput   ( 'i',"input","input","input tap file");
  //CmdArgInt    cTapLen  ( 'T',"tap","tap","tap length. Default 80");
  //CmdArgInt    cTapOff  ( 'O',"tapoffset","tapoffset","tap offset. Default 3");
  CmdArgStr    cConfig  ( 'C',"config","config","configuration file");
  CmdArgInt    cCol0    ( 'x',"col0", "number", "first column [0-511]. Default 510");
  CmdArgInt    cColN    ( 'X',"colN", "number", "last column [0-511]. Default 511");
  CmdArgInt    cRow0    ( 'y',"row0", "number", "first row [288-511]. Default 504");
  CmdArgInt    cRowN    ( 'Y',"rowN", "number", "last row [288-511]. Default 511"); 
  CmdArgInt    cMaxIter ( 'I',"maxiter", "number", "maximum number of iterations. Default 3"); 
  CmdArgBool   cShowPlot( 'p',"plot","show plot"); 

  cCol0=510;
  cColN=511;
  cRow0=504;
  cRowN=511;
  cNpulses=100;
  cMaxIter=3;
  //cTapLen=80;
  //cTapOff=3;
  
  CmdLine cmdl(*argv,
	       &cVerbose,
	       &cAddress,
	       &cChip,
	       &cTag,
	       &cNpulses,
	       &cInput,
	       //&cTapLen,
	       //&cTapOff,
	       &cConfig,
	       &cCol0,
	       &cColN,
	       &cRow0,
	       &cRowN,
	       &cMaxIter,
	       &cShowPlot,
	       NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);


  //making output directory
  string outdir=getenv("MALTA_DATA_PATH");
  mkdir(outdir.c_str(), S_IFDIR| S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  outdir+="/Malta2";
  mkdir(outdir.c_str(), S_IFDIR| S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  outdir+="/Results_TapCalib";
  mkdir(outdir.c_str(), S_IFDIR| S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  outdir+="/"+string(cChip);
  mkdir(outdir.c_str(), S_IFDIR| S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  outdir+="/"+(cTag.flags() & CmdArg::GIVEN?string(cTag):"test");
  mkdir(outdir.c_str(), S_IFDIR| S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  
  int tapLength = 80;
  int tapOffset =  3;
  int sampleLength = 390;
  int lowerLimit  = 10;
  
  Malta2 * malta = new Malta2();
  malta->Connect(string(cAddress));
  malta->SetVerbose(false);

  cout << "#####################################" << endl
       << "# Configure the chip                #" << endl
       << "#####################################" << endl;
  
  Malta2Utils::PreConfigureMalta(malta);
  Malta2Utils::EnableAll(malta);
  if (cConfig.flags()){
    if(!malta->SetConfigFromFile(string(cConfig))){
      cout << "## Cannot parse config file: " << string(cConfig) << endl;
      cout << "## Exit"<<endl;
      delete malta;
      exit(0);
    }
  }
  else{
    cout << "##  using default config from Malta2Utils" << endl;
  }
  

  //Build the masker
  Malta2Masker masker;
  masker.SetVerbose(cVerbose);
  masker.SetRange(cCol0,cRow0,cColN+1,cRowN+1);
  masker.SetShape(2,8);
  masker.SetFrequency(1);
  masker.Build();

  uint32_t iteration=0;
  uint32_t step=0;
  vector<pair<uint32_t,uint32_t> > taps(37,make_pair(0,0));
  vector<float> differences(37,0);
  vector<float> corrections(37,0);
  ostringstream os;

  TFile *tout = new TFile((outdir+"/results_tapcalib.root").c_str(),"RECREATE");
  ofstream fout((outdir+"/results_tapcalib.txt").c_str());
  
  TTree *ttap = new TTree("tap_calibration","tap_calibration");
  ttap->Branch("step",&step,"step/I");
  ttap->Branch("iteration",&iteration,"iteration/I");
  //ttap->Branch("taps",&taps);
  ttap->Branch("differences",&differences);
  ttap->Branch("corrections",&corrections);

  cout << "#####################################" << endl
       << "# Start Pixel Loop                  #" << endl
       << "#####################################" << endl;

  for(step=0; step<masker.GetNumSteps(); step++){
  
    cout << "Get step" << endl;
    masker.GetStep(step);
    cout << "Mask and unmask" << endl;
    for(uint32_t p=0; p<512; p++) {
      malta->SetPixelPulseRow(p,false);
      malta->SetPixelPulseColumn(p,false);
      malta->SetPixelMaskRow(p,true);
      malta->SetPixelMaskColumn(p,true);
      malta->SetPixelMaskDiag(p,true);
    }
    for(uint32_t a=0;a<256;a++){
      malta->SetDoubleColumnMask(a,true);
    }
    for(auto pixel: masker.GetPixels()){
      malta->SetPixelPulse(pixel.first,pixel.second,true);
      malta->SetPixelMask (pixel.first,pixel.second,false);
      os.str("");
      os << "# Selected pixel: " << pixel.first << ", " << pixel.second << endl;
      cout << os.str(); fout << os.str();
    }
    for(auto dc: masker.GetDoubleColumns()){
      malta->SetDoubleColumnMask(dc,false);
    }
    if(cInput.flags() & CmdArg::GIVEN){
      cout << "Reading taps from file: " << cInput << endl;
      ifstream fr(cInput);
      while(fr.good()){
	uint32_t chn, tap1, tap2;
	fr >> chn >> tap1 >> tap2;
	taps[chn].first=tap1;
	taps[chn].second=tap2;	
      }      
    }else{
      cout << "Writing default taps" << endl;
      for(uint32_t i=0;i<37;i++){
	taps[i].first=13;
	taps[i].second=10;
      }
    }       
    
    Malta2Utils::MaltaSend(malta);
  
    for(iteration=0;iteration<=cMaxIter;iteration++){
      
      os.str("");
      os << "#Iteration: " << (iteration+1) << endl;
      cout << os.str(); fout << os.str();
      
      //#############################################################
      //# Write the Taps ############################################
      //#############################################################
      
      os.str("");
      os << "# Tap values (bit, tap1, tap2): " << endl;
      for (int chn=0; chn < 37; chn++){
	malta->WriteTap(chn, taps[chn].first, taps[chn].second);
	os << setw(2) << chn << "  "
	   << setw(2) << taps[chn].first << "  "
	   << setw(2) << taps[chn].second << endl; 
      }
      cout << os.str(); fout << os.str();
      
      //#############################################################
      //# Readout ###################################################
      //#############################################################
            
      malta->ReadoutOn();
      malta->ResetFifo(); 
      malta->Trigger(cNpulses,true);
      malta->ReadoutOff();
      
      malta->ReadFifoStatus();
      bool fifoMonEmpty = malta->IsFifoMonEmpty();
      int nEntry = 0;
      uint32_t monitor[37];

      os.str("");
      os << "sample_vs_bit_" << step << "_" << iteration;
      TH2D * h1= new TH2D(os.str().c_str(),";sample;bit",16,0,16,37,0,37);
      
      while ( fifoMonEmpty ==false && nEntry<200){
	malta->ReadMonitorWord(&monitor[0]);
	if (monitor[0]==0){ 
	  cout << "Empty word ... this should never happen!!" << endl;
	  break;
	}
	uint32_t start=16;
	for (uint32_t bit=0; bit<16;bit++){
	  if (((monitor[0]>>bit)&0x1)==1){
	    start=bit;
	    break;
	  }
	}
	if(start==16){
	  cout << "No valid reference pulse. try again" << endl;
	  malta->ReadFifoStatus();
	  fifoMonEmpty = malta->IsFifoMonEmpty();
	  //continue;
	}
	for (uint32_t w=0; w<37; w++){
	  for (uint32_t bit=0; bit<16;bit++){
	    if (((monitor[w]>>bit)&0x1)==0) continue;
	    h1->Fill(bit-start+1,w);
	  }
	}     
	nEntry+=1;
	malta->ReadFifoStatus();
	fifoMonEmpty = malta->IsFifoMonEmpty();
      }
      
      cout << "Number of collected hits: " << nEntry << endl;
      h1->Write();

      vector<TLine*>  lines;
      lines.push_back(new TLine(0, 1,16, 1));
      lines.push_back(new TLine(0,17,16,17));
      lines.push_back(new TLine(0,22,16,22));
      lines.push_back(new TLine(0,23,16,23));
      lines.push_back(new TLine(0,26,16,26));
      lines.push_back(new TLine(0,34,16,34));
      lines.push_back(new TLine(0,36,16,36));
      
      os.str("");
      os << "c1_" << step << "_" << iteration;
      TCanvas *c1= new TCanvas(os.str().c_str(),"c1",600,900);
      gStyle->SetOptStat(0);
      h1->Draw("COLZtext");
      for (auto line: lines){
	line->SetLineWidth(1);
	line->SetLineStyle(2);
	line->Draw("SAMEL");
      } 
      c1->Print((outdir+"/"+os.str()+".pdf").c_str());
      if (cShowPlot) system(("gv "+outdir+"/"+os.str()+".pdf &").c_str());

      
      //#############################################################
      //# Calculating Distance to Reference #########################
      //#############################################################

      cout << "# Calculate distance to reference" << endl;
      
      vector<float> meanX(37,0);
      vector<int> valX(37,0);
      vector<int> sumX(37,0);
      vector<bool> valid(37,true);
      bool too_large, too_few;
      for (uint32_t chn=0; chn<37; chn++){
	too_large=false;
	too_few=false;
	valid[chn]=true;
	for (uint32_t x=0 ;x<h1->GetNbinsX(); ++x){
	  //stability protection at least 5 hits per bin
	  if (h1->GetBinContent(x+1,chn+1)<lowerLimit) continue;
	  valX[chn]+=h1->GetBinContent(x+1,chn+1)*(x+1); //ANDREA why +1??
	  sumX[chn]+=h1->GetBinContent(x+1,chn+1);
	}
	//if(sumX[0]==0){break;}
	meanX[chn]=(sumX[chn]==0?0:valX[chn]/(float)sumX[chn]);
 	if(sumX[chn]<0.3*sumX[0]){meanX[chn]=0;too_few=true;valid[chn]=false;}
	differences[chn] = (sampleLength*(meanX[0]-meanX[chn]));
	corrections[chn] = round(differences[chn]/tapLength);
	if(fabs(corrections[chn])>15 and !too_few){corrections[chn]=0;too_large=true;valid[chn]=false;}
	os.str("");
	os << "chn: " << setw(2) << chn << " "
	   << "meanX: " << setw(7) << meanX[chn] << " "
	   << "difference: " << setw(8) << differences[chn] << " ps "
	   << "correction: " << setw(4) << corrections[chn] << " taps "
	   << (too_large?" (distance was too large to correct)":"")
	   << (too_few?" (too few hits compared to reference)":"")
	   << endl;
	cout << os.str(); fout << os.str();
      }
      
      ttap->Fill();

      if(sumX[0]==0){
	os.str("");
	os << "reference bit does not have hits. Try again..." << endl;
	cout << os.str(); fout << os.str();
	continue;
      }
      
      //#############################################################
      //# Decide if it's worth scanning again #######################
      //#############################################################
      
      uint32_t num_ok=0;
      uint32_t num_val=0;
      uint32_t num_med=0;
      uint32_t num_bad=0;
      for(uint32_t chn=1;chn<37;chn++){
	if(!valid[chn]){continue;}
	if(fabs(corrections[chn])<=1){num_ok++;}
	else if(fabs(corrections[chn])>1 and fabs(corrections[chn])<=10){num_med++;}
	else{num_bad++;}
	num_val++;
      }
      os.str("");
      os << "Number of valid corrections: " << num_val << endl
	 << "Number corrections [ 0,  1]: " << num_ok << endl
	 << "Number corrections ] 1, 10]: " << num_med << endl
	 << "Number corrections ]10,inf]: " << num_bad << endl;
      cout << os.str();
      fout << os.str();
      if((float)num_ok>(0.8*(float)num_val)){
	os.str("");
	os << "Too few changes to iterate again" << endl;
	cout << os.str();
	fout << os.str();
	break;
      }

      cout << "# Set the taps for the next iteration " << endl;
      for (int chn=0; chn < 37; chn++){
	vector<uint32_t> pval=malta->ReadTap(chn);
	int32_t val1=pval[0]+corrections[chn];
	int32_t val2=val1-tapOffset;
	if     (val1>=0 and val2<0){val1-=val2; val2=0;}
	else if(val1<0 and val2<0){val1=pval[0];val2=pval[1];}
	taps[chn].first=val1;
	taps[chn].second=val2;
      }

      
    }
      
  }

  cout << "cleaning the house" << endl;
  delete malta;
  
  cout << "Output directory: " << outdir << endl;
  cout << "Results written to: " << (outdir+"/results_tapcalib.root") << endl;
  ttap->Write();
  tout->Close();
  delete tout;
  
  cout << "Have a nice day" << endl;
  return 0;

}
