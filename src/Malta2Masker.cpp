#include "Malta2/Malta2Masker.h"
#include <set>
#include <algorithm>
#include <iostream>

using namespace std;

Malta2Masker::Malta2Masker(){
  m_px0=0;
  m_pxN=511;
  m_py0=288;
  m_pyN=511;
  m_size_x=8;
  m_size_y=2;
  m_freq=1;
  m_cur_step=0;
  m_num_steps=0;
  m_verbose=true;
  m_doAura=false;
}

Malta2Masker::~Malta2Masker(){
  m_double_cols.clear();
  m_pixels.clear();
  m_auraPixels.clear();
}

void Malta2Masker::SetVerbose(bool verbose){
  m_verbose=verbose;
}

void Malta2Masker::SetRange(uint32_t px0, uint32_t py0, uint32_t pxN, uint32_t pyN){
  m_px0=px0;
  m_pxN=pxN;
  m_py0=py0;
  m_pyN=pyN;
}

void Malta2Masker::SetShape(uint32_t size_x, uint32_t size_y){
  if(size_x==0) size_x=1;
  if(size_x> (m_pxN-m_px0)) size_x=(m_pxN-m_px0);
  if(size_y==0) size_y=1;
  if(size_y> (m_pyN-m_py0)) size_y=(m_pyN-m_py0);
  m_size_x = size_x;
  m_size_y = size_y;
}

void Malta2Masker::SetFrequency(uint32_t freq){
  m_freq=freq;
}

void Malta2Masker::Build(){
  if(m_verbose){cout << "Malta2Masker::Build" << endl;}
  m_num_steps_y = (m_pyN-m_py0)/(float)m_size_y;
  m_num_steps_x = (m_pxN-m_px0)/(float)m_size_x;
  if(m_num_steps_y<1) m_num_steps_y=1;
  if(m_num_steps_x<1) m_num_steps_x=1;
  m_num_steps=m_num_steps_y*m_num_steps_x/m_freq;
  m_cur_step=0;
}

uint32_t Malta2Masker::GetNumSteps(){
  return m_num_steps;
}

void Malta2Masker::GetStep(uint32_t step){
  if(m_verbose) {cout << "Malta2Masker::GetStep" << endl;}
  m_pixels.clear();
  m_auraPixels.clear();
  m_double_cols.clear();
  set<uint32_t> dcs;
  for(uint32_t f=0;f<m_freq;f++){
    uint32_t step2=step+f*m_num_steps;
    uint32_t py1 = m_size_y*(step2%m_num_steps_y)+m_py0;
    uint32_t px1 = m_size_x*(step2/m_num_steps_y)+m_px0;
    if(m_verbose) cout << "Malta2Masker::GetStep start at px1:" << px1 << ", py1:" << py1 << endl;
    for(uint32_t dy=0; dy<m_size_y; dy++){
      for(uint32_t dx=0; dx<m_size_x; dx++){
	if(m_verbose) cout << "Malta2Masker::GetStep add pixel:" << (px1+dx) << ", " << (py1+dy) << endl;
	m_pixels.push_back(make_pair<uint32_t,uint32_t>(px1+dx,py1+dy));
	dcs.insert((px1+dx)/2);
	
	if(m_doAura){
	  //Aura pixels
	  if( px1+dx +1 < m_pxN){
	    m_auraPixels.push_back(make_pair<uint32_t,uint32_t>(px1+dx +1, py1+dy));
	    dcs.insert((px1+dx+1)/2);
	  }
	  if( px1+dx  > 0) {
	    m_auraPixels.push_back(make_pair<uint32_t,uint32_t>(px1+dx -1, py1+dy));
	    dcs.insert((px1+dx-1)/2);
	  }
	  if( py1+dy +1 < m_pyN){
	    m_auraPixels.push_back(make_pair<uint32_t,uint32_t>(px1+dx , py1+dy +1));
	  }
	  if( py1+dy  > 0){
	    m_auraPixels.push_back(make_pair<uint32_t,uint32_t>(px1+dx , py1+dy -1));
	  }
	}
	
      }
    }
  }
  for(auto dc : dcs){
    if(m_verbose) cout << "Malta2Masker::GetStep add dc:" << dc << endl;
    m_double_cols.push_back(dc);
  }
}

void Malta2Masker::GetNextStep(){
  if(m_cur_step>=m_num_steps) return;
  m_cur_step++;
  GetStep(m_cur_step);
}

vector<uint32_t> & Malta2Masker::GetDoubleColumns(){
  return m_double_cols;
}

vector<pair<uint32_t,uint32_t> > & Malta2Masker::GetPixels(){
  return m_pixels;
}
vector<pair<uint32_t,uint32_t> > & Malta2Masker::GetAuraPixels(){
  return m_auraPixels;
}

bool Malta2Masker::Contains(uint32_t col, uint32_t row){
  pair<uint32_t,uint32_t> pp(col,row);
  return (std::find(m_pixels.begin(), m_pixels.end(), pp) != m_pixels.end());
}

