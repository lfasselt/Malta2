//! -*-C++-*-
#include "Malta2/Malta2.h"
#include "Malta2/Malta2Data.h"
#include "Malta2/Malta2Tree.h"
#include "Malta2/Malta2Utils.h"
#include "TCanvas.h"
#include "TStyle.h"
#include <cmdl/cmdargs.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <signal.h>
#include <iomanip>
#include <chrono>
#include <time.h>
#include <algorithm>
#include <unistd.h>
#include <sys/stat.h>
#include <set>

#include "TCanvas.h"
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TLine.h"
#include "TGraphErrors.h"
#include "TStyle.h"
#include "TLatex.h"
#include "TFile.h"
#include "TTree.h"
#include "TLegend.h"

using namespace std;
bool g_cont=true;

void handler(int) { 
  cout << "You pressed ctrl+c to quit" << endl;
  g_cont=false;
}

int main(int argc, char *argv[]){
    
  cout << "#####################################" << endl
       << "# Welcome to MALTA2 Noise Scan      #" << endl
       << "#####################################" << endl;
  
  CmdArgBool    cVerbose( 'v',"verbose","turn on verbose mode");
  CmdArgBool    cQuiet(   'q',"quiet","do not write root files");
  CmdArgStr     cAddress( 'a',"address","address","connection string. upd://host:port",CmdArg::isREQ);
  CmdArgStr     cChip(    'c',"chip","sample","WXRX. Required",CmdArg::isREQ);
  CmdArgStr     cTag(     't',"tag","text","test description. Defatult test");
  CmdArgStr     cConfig(  'C',"config","config","configuration file");
  CmdArgInt     cTrigs(   'n',"ntrigs","number","number of triggers per DAQ window (2.5us). Default 10");
  CmdArgInt     cReps(    'N',"nreps","number","number of repetitions. Default 50");
  CmdArgInt     cPixStep( 'p',"pixstep","number","Max pixels per step. Default 5");
  CmdArgInt     cPixMax(  'P',"pixmax","number","Max pixels per scan. Default 300");
  CmdArgFloat   cRateMax( 'R',"ratemax","kilohertz","Max noise rate in kHz. Default 1");
  CmdArgInt     cIterMax( 'I',"iterations","number","Max iterations. Default 200");

  cTrigs = 10;
  cReps = 50;
  cPixStep = 5;
  cPixMax = 300;
  cRateMax = 1.0;
  cIterMax = 200;
  
  CmdLine cmdl(*argv,
	       &cVerbose,
	       &cQuiet,
	       &cAddress,
	       &cChip,
	       &cTag,
	       &cConfig,
	       &cTrigs,
	       &cReps,
	       &cPixStep,
	       &cPixMax,
	       &cRateMax,
	       &cIterMax,
	       NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);

  //making output directory
  string outdir=getenv("MALTA_DATA_PATH");
  mkdir(outdir.c_str(), S_IFDIR| S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  outdir+="/Malta2";
  mkdir(outdir.c_str(), S_IFDIR| S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  outdir+="/Results_NoiseScan";
  mkdir(outdir.c_str(), S_IFDIR| S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  outdir+="/"+string(cChip);
  mkdir(outdir.c_str(), S_IFDIR| S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  outdir+="/"+(cTag.flags() & CmdArg::GIVEN?string(cTag):"test");
  mkdir(outdir.c_str(), S_IFDIR| S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  if(!cQuiet) mkdir((outdir+"/hits").c_str(), S_IFDIR| S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  
  //Connect to MALTA2
  Malta2 * malta = new Malta2();
  malta->Connect(string(cAddress));

  malta->SetVerbose(false);
  malta->WriteConstDelays(4,1);
  malta->SetExternalL1A(false);
  malta->SetReadoutDelay(80);
  
  cout << "#####################################" << endl
       << "# Configure the chip                #" << endl
       << "#####################################" << endl;

  Malta2Utils::PreConfigureMalta(malta);
  Malta2Utils::EnableAll(malta);
  if (cConfig.flags()){
    if(!malta->SetConfigFromFile(string(cConfig))){
      cout << "## Cannot parse config file: " << string(cConfig) << endl;
      cout << "## Exit"<<endl;
      delete malta;
      exit(0);
    }
  }
  else{
    cout << "##  using default config from Malta2Utils" << endl;
    //malta->SetIDB(80);
    //malta->SetITHR(80);
  }
  
  cout << "#####################################" << endl
       << "# Start Rate Loop                   #" << endl
       << "#####################################" << endl;


  time_t scan_start,scan_stop;
  time_t loop_start,loop_stop;
   
  float kilo=1000;
  float DAQwindow = 2500e-9;
  float totTime = cTrigs*DAQwindow*cReps;
  float noiseRate = 10000;
  uint32_t numPixelsMasked = 0;
  uint32_t iteration=0;
  
  vector<TH2I*> vnHits_h2;
  TGraphErrors * gNoiseVsMasked = new TGraphErrors();
  gNoiseVsMasked->SetNameTitle("noise_vs_masked",";Pixels masked [#]; Chip noise rate [kHz]");
  TGraphErrors * gHottestVsMasked = new TGraphErrors();
  gHottestVsMasked->SetNameTitle("hottest_vs_masked",";Pixels masked [#]; Hottest pixel noise rate [kHz]");
  vector<pair<uint32_t,uint32_t> > maskedPixels;
  
  signal(SIGINT,handler);
  time(&scan_start);

  while(true){

    if(!g_cont){break;}
    if(noiseRate < cRateMax){
      cout << "# Max noise reached" << endl;
      break;
    }
    if(iteration==cIterMax){
      cout << "# Max iterations reached" << endl;
      break;
    }
    if(numPixelsMasked==cPixMax){
      cout << "# Max masked pixels reached" << endl;
      break;
    }
    time(&loop_start);
    
    Malta2Utils::MaltaSend(malta);
    
    cout << "==> Start iteration: " << iteration << endl;

    TH2I* nHits_h2 = new TH2I(("Hits_"+to_string(iteration)).c_str(),"Occupancy;column;row;counts",512,-0.5,511.5,512,-0.5,511.5);
    vnHits_h2.push_back(nHits_h2);
    
    //Create ntuple
    Malta2Tree *ntuple = new Malta2Tree();
    if (!cQuiet){
      ostringstream os;
      os << outdir  << "/hits/noisescan_" << setw(3) << setfill('0') << iteration << ".root";
      ntuple->Open(os.str(),"RECREATE");
    }
    
    //repetitions
    for( int r=0; r<cReps; r++) {
      if(!g_cont){break;}
      
      //Flush MALTA
      malta->ResetFifo();
      malta->ResetL1Counter();
      malta->ReadoutOn();
      malta->Trigger(cTrigs,false);
      malta->ReadoutOff();

      Malta2Data md,md2;
      int maxwords=200;
      uint32_t words[200];
      for (uint32_t i=0;i<maxwords;i++){ words[i]=0;}
      
      bool isFirst=true;
      int numberOfHits=0;
      
      //Readout 
      while(g_cont){	      
	int numwords = malta->GetFIFO2WordCount();
	if(numwords > maxwords){ numwords = maxwords; }
	
	malta->ReadMaltaWord(words,numwords);
	
	// This is the way out of the loop.
	if (words[0]==0){ break; }
	
	for (uint32_t i = 0; i < numwords; i+=2){		      
	  if (isFirst){
	    md.setWord1(words[0]);
	    md.setWord2(words[1]);
	    isFirst = false;
	    continue;
	  }
	  
	  md2.setWord1(md.getWord1());
	  md2.setWord2(md.getWord2());
	  md.setWord1(words[0+i]);
	  md.setWord2(words[1+i]);
	  md.unPack();
	  md2.unPack();
	  
	  bool markDuplicate = Malta2Utils::markDuplicate(md2.getBcid(),md.getBcid(),
							  md2.getWinid(),md.getWinid(),
							  md2.getL1id(),md.getL1id(),
							  md2.getPhase(),md.getPhase());
	  
	  if (!cQuiet){
	    ntuple->Set(&md2);
	    ntuple->SetIsDuplicate(markDuplicate?1:0);
	    ntuple->Fill();
	  }
	  if (markDuplicate==0 ){ 
	    for (unsigned h=0; h<md2.getNhits(); h++){
	      uint32_t pixX=md2.getHitColumn(h);
	      uint32_t pixY=md2.getHitRow(h);
	      if(cVerbose) cout << "hit: " << pixX << "," << pixY << endl;
	      if(malta->IsPixelMasked(pixX,pixY)){
		if(cVerbose){cout << "Pixel should be masked: " << pixX << ", " << pixY
				  << " Check if the threshold is too low and the hits are being merged" << endl;}
	      }
	      numberOfHits++;
	      nHits_h2->Fill(pixX, pixY);	      
	    }
	  }
	}
      }
     
      cout << "Number of hits in selected Pixels: " << numberOfHits << endl;
      
      if (numberOfHits > 500000) {
	cout << "The FIFO is full, aborting scan" << endl;
	g_cont=false;
	break; 
      } 
    } //end of repetition loop

    time(&loop_stop);
    cout << "Elapsed time [s]: " << (loop_stop-loop_start) << endl;
    
    if (!cQuiet) ntuple->Close();
    delete ntuple;

    noiseRate = (float)nHits_h2->GetEntries()/totTime/kilo;
    float hottestRate = 0;
    
    if(noiseRate < cRateMax){
      cout << "# Max noise reached" << endl;
      break;
    }
    
    ostringstream msg;
    ofstream fout(outdir+"/Mask_"+to_string(iteration)+".txt"); 
    //fixme: copy the config file contents if given
    if(cConfig.flags()&&CmdArg::GIVEN){}
    msg << "#The noise rate of the entire chip is: " << noiseRate << " kHz";
    cout << msg.str() << endl;
    fout << msg.str() << endl;
    
    //Loop over the pixels
    uint32_t pix_hits=0;
    pair<uint32_t,uint32_t> pix_sel(512,512);
    set<pair<uint32_t, uint32_t> > vpix;
    uint32_t masked=0;
    while(true){
      if(masked>=cPixStep){break;}
      pix_hits=0;
      for (int32_t col=0; col<nHits_h2->GetNbinsX(); col++){
	for (int32_t row=0; row<nHits_h2->GetNbinsY(); row++){
	  pair<uint32_t,uint32_t> pix_test(col,row);
	  if(nHits_h2->GetBinContent(col+1,row+1)>pix_hits and !vpix.contains(pix_test)){
	    pix_sel=pix_test;
	    pix_hits=nHits_h2->GetBinContent(col+1,row+1);
	  }
	}
      }
      if(pix_hits==0) break;
      if(masked==0){hottestRate=(float)pix_hits/totTime/kilo;}
      vpix.insert(pix_sel);
      if(malta->IsPixelMasked(pix_sel.first,pix_sel.second)){
	if(cVerbose) cout << "Pixel should be masked already: " << pix_sel.first << ", " << pix_sel.second << endl;
	continue;
      }
      //find the malta word based on the pixel coordinates, print this as along wirh rate and nhits
      Malta2Data testData;
      testData.setHit(pix_sel.first,pix_sel.second);
      msg.str("");
      msg << "MASK_PIXEL: " << pix_sel.first << ", " << pix_sel.second << " \t# entries " << setw(5) << pix_hits
	    << " rate " << setw(7) << fixed << setprecision(1) << pix_hits/totTime/kilo << " kHz"
	    << " word \t" << testData.toString();
      cout << msg.str() << endl;
      fout << msg.str() << endl;
      //Mask MALTA for the next iteration
      malta->SetPixelMask(pix_sel.first,pix_sel.second,true);
      maskedPixels.push_back(pix_sel);
      masked++;
    }

    if(noiseRate==0){noiseRate=1e-8;}
    if(hottestRate==0){hottestRate=1e-8;}
    
    //Fill histograms
    gNoiseVsMasked->SetPoint(iteration,numPixelsMasked,noiseRate);
    gNoiseVsMasked->SetPointError(iteration,0,sqrt(noiseRate));
    gHottestVsMasked->SetPoint(iteration,numPixelsMasked,hottestRate);
    gHottestVsMasked->SetPointError(iteration,0,sqrt(hottestRate));

    //update number of masked pixels
    numPixelsMasked+=masked;

    iteration++;
  }
  
  time(&scan_stop);
  cout << "Scan time [s]: " << (scan_stop-scan_start) << endl;
  cout << "Noise rate: " << noiseRate << endl;
  cout << "Number of iterations: " << iteration << endl;
  cout << "Number of pixels masked: " << numPixelsMasked << endl;
  cout << "Masked pixels:" << endl;
  for(auto pix: maskedPixels){
    cout << "MASK_PIXEL: " << pix.first << ", " << pix.second << endl;
  }
  
  cout << "Cleaning the house" << endl;
  delete malta;

  cout << "Print out some plots" << endl;
  TCanvas *can = new TCanvas("can", "can", 800, 600);
  can->SetLogy(1);
  gNoiseVsMasked->Draw("APL");
  can->Print((outdir+"/NoiseVsMasked.pdf").c_str());
  gHottestVsMasked->Draw("APL");
  can->Print((outdir+"/HottestVsMasked.pdf").c_str());
  delete can;

  cout << "Write root file" << endl;
  TFile * tout = new TFile( (outdir+"/noise_scan_results.root").c_str(), "RECREATE");
  tout->cd();
  gNoiseVsMasked->Write();
  gHottestVsMasked->Write();
  for(TH2I* h2: vnHits_h2){h2->Write();}
  tout->Close();
  delete tout;
  
  cout << "Have a nice day" << endl;
  return 0;
}


